﻿using AutoMapper;
using Jumping.Domain.Abstractions.Repositories;
using Jumping.Domain.Abstractions.Services;
using Jumping.Domain.Common.DTO;
using Jumping.Domain.Common.Exceptions;
using Jumping.Domain.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Jumping.Services.Services
{
    public class TicketService : ITicketService
    {
        private readonly ITicketRepository _ticketRepository;
        private readonly IUserRepository _userRepository;

        private readonly IMapper _mapper;

        public TicketService(ITicketRepository ticketRepository, IMapper mapper, IUserRepository userRepository)
        {
            _ticketRepository = ticketRepository;
            _mapper = mapper;
            _userRepository = userRepository;
        }

        public async Task<string> AddNewTicketAsync(CreateOrUpdateTicketDTO request)
        {
            if (request is null)
            {
                throw new AppException($"Request cannot be empty", 400);
            }

            var fromuser = _userRepository.GetByIdAsync(request.FromUserId);

            if (fromuser is null)
            {
                throw new AppException($"Student with {request.FromUserId} doesn't exist", 400);
            }

            var createTicket = _mapper.Map<Ticket>(request);

            var newId = await _ticketRepository.CreateAsync(createTicket);

            return $"Ticket was successfuly created with id {newId}.";
        }

        public async Task<ICollection<TicketResponseDTO>> GetAllTicketsAsync()
        {
            var tickets = await _ticketRepository.GetAllAsync();

            return _mapper.Map<ICollection<TicketResponseDTO>>(tickets);
        }

        public async Task<TicketResponseDTO> GetTicketByIdAsync(Guid Id)
        {
            var ticket = await _ticketRepository.GetByIdAsync(Id);

            if (ticket == null)
            {
                throw new AppException($"Ticket with {Id} wasn't found", 500);
            }

            return _mapper.Map<TicketResponseDTO>(ticket);
        }

        public async Task<string> RemoveTicketAsync(Guid id)
        {
            var ticket = await _ticketRepository.GetByIdAsync(id);

            if (ticket == null)
            {
                throw new AppException($"Ticket with {id} wasn't found", 500);
            }

            await _ticketRepository.DeleteAsync(ticket);

            return $"Ticket with {id} was succefully removed";
        }

        public async Task<string> UpdateTicketAsync(Guid id, CreateOrUpdateTicketDTO ticketDto)
        {
            var ticket = await _ticketRepository.GetByIdAsync(id);

            if (ticket == null)
            {
                throw new AppException($"Ticket with {id} wasn't found", 500);
            }

            var updatedTicket = _mapper.Map<CreateOrUpdateTicketDTO, Ticket>(ticketDto, ticket);

            await _ticketRepository.UpdateAsync(updatedTicket);

            return $"Ticket with {id} was succefully updated";
        }
    }
}
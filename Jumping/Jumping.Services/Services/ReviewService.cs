﻿using AutoMapper;
using Jumping.Domain.Abstractions.Repositories;
using Jumping.Domain.Abstractions.Services;
using Jumping.Domain.Common.DTO;
using Jumping.Domain.Common.Exceptions;
using Jumping.Domain.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Jumping.Services.Services
{
    public class ReviewService : IReviewService
    {
        private readonly IReviewRepository _reviewRepository;

        //private readonly IStudentRepository _studentRepository;
        //private readonly ITeacherRepository _teacherRepository;
        private readonly IUserRepository _userRepository;
        private readonly IMapper _mapper;

        public ReviewService(IReviewRepository reviewRepository, IMapper mapper, IUserRepository userRepository)
        {
            _reviewRepository = reviewRepository;
            _mapper = mapper;
            _userRepository = userRepository;
            //_studentRepository = studentRepository;
            //_teacherRepository = teacherRepository;
        }

        public async Task<string> AddNewReviewAsync(CreateOrUpdateReviewDTO request)
        {
            if (request is null)
            {
                throw new AppException($"Request cannot be empty", 400);
            }

            var fromuser = _userRepository.GetByIdAsync(request.AutherId);

            if (fromuser is null)
            {
                throw new AppException($"User with {request.AutherId} doesn't exist", 400);
            }

            var touser = _userRepository.GetByIdAsync(request.OnWhomId);

            if (touser is null)
            {
                throw new AppException($"User with {request.OnWhomId} doesn't exist", 400);
            }

            var createReview = _mapper.Map<Review>(request);

            var newId = await _reviewRepository.CreateAsync(createReview);

            return $"Review was successfuly created with id {newId}.";
        }

        public async Task<ICollection<ReviewResponseDTO>> GetAllReviewsAsync()
        {
            var reviews = await _reviewRepository.GetAllReviewsWithTeachersStudentsAsync();

            return _mapper.Map<ICollection<ReviewResponseDTO>>(reviews);
        }

        public async Task<ReviewResponseDTO> GetReviewByIdAsync(Guid Id)
        {
            var review = await _reviewRepository.GetReviewWithTeacherStudentByIdAsync(Id);

            if (review == null)
            {
                throw new AppException($"Review with {Id} wasn't found", 500);
            }

            return _mapper.Map<ReviewResponseDTO>(review);
        }

        public async Task<string> RemoveReviewAsync(Guid id)
        {
            var review = await _reviewRepository.GetByIdAsync(id);

            if (review == null)
            {
                throw new AppException($"Review with {id} wasn't found", 500);
            }

            await _reviewRepository.DeleteAsync(review);

            return $"Review with {id} was succefully removed";
        }

        public async Task<string> UpdateReviewAsync(Guid id, CreateOrUpdateReviewDTO reviewDTO)
        {
            var review = await _reviewRepository.GetReviewForUpdateAsync(id);

            if (review == null)
            {
                throw new AppException($"Review with {id} wasn't found", 500);
            }

            var updatedReview = _mapper.Map<CreateOrUpdateReviewDTO, Review>(reviewDTO, review);

            await _reviewRepository.UpdateAsync(updatedReview);

            return $"Review with {id} was succefully updated";
        }
    }
}
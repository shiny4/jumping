﻿using AutoMapper;
using Jumping.Domain.Abstractions.Repositories;
using Jumping.Domain.Abstractions.Services;
using Jumping.Domain.Common.DTO;
using Jumping.Domain.Common.Exceptions;
using Jumping.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jumping.Services.Services
{
    public class ApplicationService : IApplicationService
    {
        private readonly IApplicationRepository _applicationRepository;
        private readonly IMapper _mapper;

        public ApplicationService(IApplicationRepository applicationRepository, IMapper mapper)
        {
            _applicationRepository = applicationRepository;
            _mapper = mapper;
        }

        public async Task<string> AddApplicationAsync(CreateOrUpdateApplicationDTO request)
        {
            if (request is null)
            {
                throw new AppException($"Request cannot be empty", 400);
            }

            var newApplication = _mapper.Map<Application>(request);

            var newId = await _applicationRepository.CreateAsync(newApplication);

            return $"Application was successfuly created with id {newId}.";
        }

        public async Task<ICollection<ApplicationResponseDTO>> GetAllApplications()
        {
            var applications = await _applicationRepository.GetAllAsync();

            return _mapper.Map<List<ApplicationResponseDTO>>(applications);
        }

        public async Task<ApplicationResponseDTO> GetApplicationByID(Guid id)
        {
            var admin = await _applicationRepository.GetByIdAsync(id);
            return _mapper.Map<ApplicationResponseDTO>(admin);
        }

        public async Task RemoveApplicationAsync(Guid id)
        {
            var application = await _applicationRepository.GetByIdAsync(id);

            if (application == null)
            {
                throw new AppException($"Application with {id} wasn't found", 500);
            }

            await _applicationRepository.DeleteAsync(application);
        }

        public async Task UpdateApplicationAsync(Guid id, CreateOrUpdateApplicationDTO request)
        {
            if (request is null)
            {
                throw new AppException($"Request cannot be empty", 400);
            }

            var exsistingApplication = await _applicationRepository.GetByIdAsync(id);

            if (exsistingApplication is null)
            {
                throw new AppException($"Application with {id} doesn't exsists", 400);
            }

            var updateApplication = _mapper.Map(request, exsistingApplication);

            await _applicationRepository.UpdateAsync(updateApplication);
        }
    }
}

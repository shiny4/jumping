﻿using AutoMapper;
using Jumping.Domain.Abstractions.Repositories;
using Jumping.Domain.Abstractions.Services;
using Jumping.Domain.Common.DTO;
using Jumping.Domain.Common.Exceptions;
using Jumping.Domain.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Jumping.Services.Services
{
    public class SiteMessageService : ISiteMessageService
    {
        private readonly ISiteMessageRepository _siteMessageRepository;
        private readonly IUserRepository _userRepository;
        private readonly IMapper _mapper;

        public SiteMessageService(ISiteMessageRepository siteMessageRepository, IMapper mapper,
            IUserRepository userRepository)
        {
            _siteMessageRepository = siteMessageRepository;
            _mapper = mapper;
            _userRepository = userRepository;
        }

        public async Task<string> AddNewSiteMessageAsync(CreateOrUpdateSiteMessageDTO request)
        {
            if (request is null)
            {
                throw new AppException($"Request cannot be empty", 400);
            }

            var userfrom = _userRepository.GetByIdAsync(request.FromUserId);

            if (userfrom is null)
            {
                throw new AppException($"Student with {request.FromUserId} doesn't exist", 400);
            }

            var userto = _userRepository.GetByIdAsync(request.ToUserId);

            if (userto is null)
            {
                throw new AppException($"Teacher with {request.ToUserId} doesn't exist", 400);
            }


            var createSiteMessage = _mapper.Map<SiteMessage>(request);

            var newId = await _siteMessageRepository.CreateAsync(createSiteMessage);

            return $"SiteMessage was successfuly created with id {newId}.";
        }

        public async Task<ICollection<SiteMessageResponseDTO>> GetAllSiteMessagesAsync()
        {
            var siteMessages = await _siteMessageRepository.GetAllAsync();

            return _mapper.Map<ICollection<SiteMessageResponseDTO>>(siteMessages);
        }

        public async Task<SiteMessageResponseDTO> GetSiteMessageByIdAsync(Guid Id)
        {
            var siteMessage = await _siteMessageRepository.GetByIdAsync(Id);

            if (siteMessage == null)
            {
                throw new AppException($"SiteMessage with {Id} wasn't found", 500);
            }

            return _mapper.Map<SiteMessageResponseDTO>(siteMessage);
        }

        public async Task<string> RemoveSiteMessageAsync(Guid id)
        {
            var siteMessage = await _siteMessageRepository.GetByIdAsync(id);

            if (siteMessage == null)
            {
                throw new AppException($"SiteMessage with {id} wasn't found", 500);
            }

            await _siteMessageRepository.DeleteAsync(siteMessage);

            return $"SiteMessage with {id} was succefully removed";
        }

        public async Task<string> UpdateSiteMessageAsync(Guid id, CreateOrUpdateSiteMessageDTO siteMessageDto)
        {
            var siteMessage = await _siteMessageRepository.GetByIdAsync(id);

            if (siteMessage == null)
            {
                throw new AppException($"SiteMessage with {id} wasn't found", 500);
            }

            var updatedSiteMessage =
                _mapper.Map<CreateOrUpdateSiteMessageDTO, SiteMessage>(siteMessageDto, siteMessage);

            await _siteMessageRepository.UpdateAsync(updatedSiteMessage);

            return $"SiteMessage with {id} was succefully updated";
        }
    }
}
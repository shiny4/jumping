﻿using AutoMapper;
using Jumping.Domain.Abstractions.Repositories;
using Jumping.Domain.Abstractions.Services;
using Jumping.Domain.Common.DTO;
using Jumping.Domain.Common.Exceptions;
using Jumping.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Jumping.Services.Services
{
    public class NotificationPreferenceService : INotificationPreferenceService
    {
        private readonly INotificationPreferenceRepository _notificationPreferenceRepository;
        private readonly IUserAuthRepository _userAuthRepository;
        private readonly IMapper _mapper;

        public NotificationPreferenceService(INotificationPreferenceRepository notificationPreferenceRepository,
            IUserAuthRepository userAuthRepository, IMapper mapper)
        {
            _notificationPreferenceRepository = notificationPreferenceRepository;
            _userAuthRepository = userAuthRepository;
            _mapper = mapper;
        }

        public async Task<string> AddNewNotificationPreferenceAsync(CreateOrUpdateNotificationPreferenceDTO request)
        {
            if (request is null)
            {
                throw new AppException($"Request cannot be empty", 400);
            }

            var exsistingSubject =
                await _notificationPreferenceRepository.GetAsync(a => a.UserAuthId == request.UserAuthId);

            if (exsistingSubject?.FirstOrDefault() is not null)
            {
                throw new AppException($"Preference for {request.UserAuthId} exsists", 400);
            }

            exsistingSubject = await _notificationPreferenceRepository.GetAsync(a => a.Id == request.UserAuthId);

            if (exsistingSubject?.FirstOrDefault() is not null)
            {
                throw new AppException($"Preference for {request.UserAuthId} exsists", 400);
            }

            var exsistingSubject1 = await _userAuthRepository.GetAsync(a => a.Id == request.UserAuthId);

            if (exsistingSubject?.FirstOrDefault() is null)
            {
                throw new AppException($"User for {request.UserAuthId} must be regictered first", 400);
            }

            var createNotificationPreference = _mapper.Map<NotificationPreference>(request);
            createNotificationPreference.Id = createNotificationPreference.UserAuthId;

            var newId = await _notificationPreferenceRepository.CreateAsync(createNotificationPreference);

            return $"NotificationPreference was successfuly created with id {newId}.";
        }

        public async Task<ICollection<NotificationPreferenceResponseDTO>> GetAllNotificationPreferencesAsync()
        {
            var notificationPreferences = await _notificationPreferenceRepository.GetAllAsync();

            return _mapper.Map<ICollection<NotificationPreferenceResponseDTO>>(notificationPreferences);
        }

        public async Task<NotificationPreferenceResponseDTO> GetNotificationPreferenceByIdAsync(Guid Id)
        {
            var notificationPreference = await _notificationPreferenceRepository.GetByIdAsync(Id);

            if (notificationPreference == null)
            {
                throw new AppException($"NotificationPreference with {Id} wasn't found", 500);
            }

            return _mapper.Map<NotificationPreferenceResponseDTO>(notificationPreference);
        }

        // public async Task<string> RemoveNotificationPreferenceAsync(Guid id)
        // {
        //     var notificationPreference = await _notificationPreferenceRepository.GetByIdAsync(id);
        //
        //     if (notificationPreference == null)
        //     {
        //         throw new AppException($"NotificationPreference with {id} wasn't found", 500);
        //     }
        //
        //     await _notificationPreferenceRepository.DeleteAsync(notificationPreference);
        //
        //     return $"NotificationPreference with {id} was succefully removed";
        // }

        public async Task<string> UpdateNotificationPreferenceAsync(Guid id,
            CreateOrUpdateNotificationPreferenceDTO notificationPreferenceDto)
        {
            if (notificationPreferenceDto is null)
            {
                throw new AppException($"Request cannot be empty", 400);
            }

            if (notificationPreferenceDto.UserAuthId != id)
            {
                throw new AppException(
                    $"NotificationPreference with {id} can not be updated since request inconsistency", 400);
            }

            var notificationPreference = await _notificationPreferenceRepository.GetByIdAsync(id);

            if (notificationPreference == null)
            {
                throw new AppException($"NotificationPreference with {id} wasn't found", 500);
            }

            if (notificationPreference.Id != notificationPreferenceDto.UserAuthId ||
                notificationPreference.UserAuthId != notificationPreferenceDto.UserAuthId)
            {
                throw new AppException($"NotificationPreference with {id} can not be updated since db inconsistency",
                    400);
            }

            var updatedNotificationPreference =
                _mapper.Map<CreateOrUpdateNotificationPreferenceDTO, NotificationPreference>(notificationPreferenceDto,
                    notificationPreference);

            await _notificationPreferenceRepository.UpdateAsync(updatedNotificationPreference);

            return $"NotificationPreference with {id} was succefully updated";
        }
    }
}
﻿using AutoMapper;
using Jumping.Domain.Abstractions.Repositories;
using Jumping.Domain.Abstractions.Services;
using Jumping.Domain.Common.DTO;
using Jumping.Domain.Common.Exceptions;
using Jumping.Domain.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Jumping.Services.Services
{
    public class CanceledLessonService : ICanceledLessonService
    {
        private readonly ICanceledLessonRepository _canceledLessonRepository;
        private readonly IUserRepository _userRepository;
        private readonly ILessonRepository _lessonRepository;
        private readonly IMapper _mapper;

        public CanceledLessonService(ICanceledLessonRepository canceledLessonRepository,
            IMapper mapper, //IStudentRepository studentRepository,
            IUserRepository userRepository, ILessonRepository lessonRepository)
        {
            _canceledLessonRepository = canceledLessonRepository;
            _mapper = mapper;
            //_studentRepository = studentRepository;
            _userRepository = userRepository;
            _lessonRepository = lessonRepository;
        }

        public async Task<string> AddNewCanceledLessonAsync(CreateOrUpdateCanceledLessonDTO request)
        {
            if (request is null)
            {
                throw new AppException($"Request cannot be empty", 400);
            }

            var user = _userRepository.GetByIdAsync(request.InitiatorId);

            if (user is null)
            {
                throw new AppException($"Student with {request.InitiatorId} doesn't exist", 400);
            }

            var lesson = _lessonRepository.GetByIdAsync(request.LessonId);

            if (lesson is null)
            {
                throw new AppException($"Teacher with {request.LessonId} doesn't exist", 400);
            }

            var createCanceledLesson = _mapper.Map<CanceledLesson>(request);

            var newId = await _canceledLessonRepository.CreateAsync(createCanceledLesson);

            return $"CanceledLesson was successfuly created with id {newId}.";
        }

        public async Task<ICollection<CanceledLessonResponseDTO>> GetAllCanceledLessonsAsync()
        {
            var canceledLessons = await _canceledLessonRepository.GetAllAsync();

            return _mapper.Map<ICollection<CanceledLessonResponseDTO>>(canceledLessons);
        }

        public async Task<CanceledLessonResponseDTO> GetCanceledLessonByIdAsync(Guid Id)
        {
            var canceledLesson = await _canceledLessonRepository.GetByIdAsync(Id);

            if (canceledLesson == null)
            {
                throw new AppException($"CanceledLesson with {Id} wasn't found", 500);
            }

            return _mapper.Map<CanceledLessonResponseDTO>(canceledLesson);
        }

        public async Task<string> RemoveCanceledLessonAsync(Guid id)
        {
            var canceledLesson = await _canceledLessonRepository.GetByIdAsync(id);

            if (canceledLesson == null)
            {
                throw new AppException($"CanceledLesson with {id} wasn't found", 500);
            }

            await _canceledLessonRepository.DeleteAsync(canceledLesson);

            return $"CanceledLesson with {id} was succefully removed";
        }

        public async Task<string> UpdateCanceledLessonAsync(Guid id, CreateOrUpdateCanceledLessonDTO canceledLessonDto)
        {
            var canceledLesson = await _canceledLessonRepository.GetByIdAsync(id);

            if (canceledLesson == null)
            {
                throw new AppException($"CanceledLesson with {id} wasn't found", 500);
            }

            var updatedCanceledLesson =
                _mapper.Map<CreateOrUpdateCanceledLessonDTO, CanceledLesson>(canceledLessonDto, canceledLesson);

            await _canceledLessonRepository.UpdateAsync(updatedCanceledLesson);

            return $"CanceledLesson with {id} was succefully updated";
        }
    }
}
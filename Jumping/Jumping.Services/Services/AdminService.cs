﻿using AutoMapper;
using Jumping.Domain.Abstractions.Repositories;
using Jumping.Domain.Abstractions.Services;
using Jumping.Domain.Common.DTO;
using Jumping.Domain.Common.Exceptions;
using Jumping.Domain.Models.Administration;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Jumping.Domain.Enums;

namespace Jumping.Services.Services
{
    public class AdminService : IAdminService
    {
        private readonly IAdminRepository _adminRepository;
        private readonly IMapper _mapper;

        public AdminService(IAdminRepository adminRepository, IMapper mapper)
        {
            _adminRepository = adminRepository;
            _mapper = mapper;
        }

        public async Task<string> AddNewAdminAsync(CreateOrUpdateAdminDTO request)
        {
            if (request is null)
            {
                throw new AppException($"Request cannot be empty", 400);
            }

            var exsistingAdmins = await _adminRepository.GetAsync(a => a.Email == request.Email);

            if (exsistingAdmins.FirstOrDefault() is not null)
            {
                throw new AppException($"Exsisting admin with {request.Email} exsists", 400);
            }

            var newAdmin = _mapper.Map<Admin>(request);

            // will be updated after addin auth
            newAdmin.PasswordSalt = "345";

            var newId = await _adminRepository.CreateAsync(newAdmin);

            return $"Admin was successfuly created with id {newId}.";
        }

        public async Task<ICollection<AdminResponseDTO>> GetAllAdminsAsync()
        {
            var admins = await _adminRepository.GetAllAsync();

            return _mapper.Map<List<AdminResponseDTO>>(admins);
        }

        public async Task RemoveAdminAsync(Guid id)
        {
            var exsistingAdmin = await _adminRepository.GetByIdAsync(id);

            if (exsistingAdmin is null)
            {
                throw new AppException($"Admin with {id} doesn't exsist", 400);
            }

            await _adminRepository.DeleteAsync(exsistingAdmin);
        }

        public async Task<AdminResponseDTO> GetAdminByIDAsync(Guid id)
        {
            var admin = await _adminRepository.GetAdminAsync(id);

            if (admin == null)
            {
                throw new AppException($"Entity with {id} wasn't found", 500);
            }

            return _mapper.Map<AdminResponseDTO>(admin);
        }

        // public async Task<ICollection<AdminResponseDTO>> GetAllAdminsAsync()
        // {
        //     var admins = await _adminRepository.GetAdminsWithRolesAsync();
        //
        //     return _mapper.Map<List<AdminResponseDTO>>(admins);
        // }

        public async Task UpdateAdminAsync(Guid id, CreateOrUpdateAdminDTO request)
        {
            if (request is null)
            {
                throw new AppException($"Request cannot be empty", 400);
            }

            var exsistingAdmin = await _adminRepository.GetAdminForUpdateAsync(id);

            if (exsistingAdmin is null)
            {
                throw new AppException($"Admin with {id} doesn't exsists", 400);
            }

            var updateAdmin = _mapper.Map(request, exsistingAdmin);

            await _adminRepository.UpdateAsync(updateAdmin);
        }
    }
}
﻿using AutoMapper;
using Jumping.Domain.Abstractions.Repositories;
using Jumping.Domain.Abstractions.Services;
using Jumping.Domain.Common.DTO;
using Jumping.Domain.Common.Exceptions;
using Jumping.Domain.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Jumping.Services.Services
{
    public class LessonService : ILessonService
    {
        private readonly ILessonRepository _lessonRepository;

        //private readonly IStudentRepository _studentRepository;
        //private readonly ITeacherRepository _teacherRepository;
        private readonly IUserRepository _userRepository;
        private readonly ISubjectRepository _subjectRepository;
        private readonly IMapper _mapper;

        public LessonService(ILessonRepository lessonRepository, IMapper mapper,
            IUserRepository userRepository, ISubjectRepository subjectRepository
            //IStudentRepository studentRepository, ITeacherRepository teacherRepository
        )
        {
            _lessonRepository = lessonRepository;
            _mapper = mapper;
            _userRepository = userRepository;
            _subjectRepository = subjectRepository;
            //_studentRepository = studentRepository;
            //_teacherRepository = teacherRepository;
        }

        public async Task<string> AddNewLessonAsync(CreateOrUpdateLessonDTO request)
        {
            if (request is null)
            {
                throw new AppException($"Request cannot be empty", 400);
            }

            // var student = _userRepository.GetByIdAsync(request.);
            //
            // if (student is null)
            // {
            //     throw new AppException($"Student with {request.AutherId} doesn't exist", 400);
            // }

            var teacher = _userRepository.GetByIdAsync(request.TeacherId);

            if (teacher is null)
            {
                throw new AppException($"Teacher with {request.TeacherId} doesn't exist", 400);
            }

            var subject = _subjectRepository.GetByIdAsync(request.SubjectId);

            if (subject is null)
            {
                throw new AppException($"Subject with {request.SubjectId} doesn't exist", 400);
            }

            var createLesson = _mapper.Map<Lesson>(request);

            var newId = await _lessonRepository.CreateAsync(createLesson);

            return $"Lesson was successfuly created with id {newId}.";
        }

        public async Task<ICollection<LessonResponseDTO>> GetAllLessonsAsync()
        {
            var lessons = await _lessonRepository.GetAllAsync();

            return _mapper.Map<ICollection<LessonResponseDTO>>(lessons);
        }

        public async Task<LessonResponseDTO> GetLessonByIdAsync(Guid Id)
        {
            var lesson = await _lessonRepository.GetByIdAsync(Id);

            if (lesson == null)
            {
                throw new AppException($"Lesson with {Id} wasn't found", 500);
            }

            return _mapper.Map<LessonResponseDTO>(lesson);
        }

        public async Task<string> RemoveLessonAsync(Guid id)
        {
            var lesson = await _lessonRepository.GetByIdAsync(id);

            if (lesson == null)
            {
                throw new AppException($"Lesson with {id} wasn't found", 500);
            }

            await _lessonRepository.DeleteAsync(lesson);

            return $"Lesson with {id} was succefully removed";
        }

        public async Task<string> UpdateLessonAsync(Guid id, CreateOrUpdateLessonDTO lessonDto)
        {
            var lesson = await _lessonRepository.GetByIdAsync(id);

            if (lesson == null)
            {
                throw new AppException($"Lesson with {id} wasn't found", 500);
            }

            var updatedLesson = _mapper.Map<CreateOrUpdateLessonDTO, Lesson>(lessonDto, lesson);

            await _lessonRepository.UpdateAsync(updatedLesson);

            return $"Lesson with {id} was succefully updated";
        }
    }
}
﻿using AutoMapper;
using Jumping.Domain.Abstractions.Repositories;
using Jumping.Domain.Abstractions.Services;
using Jumping.Domain.Common.DTO;
using Jumping.Domain.Common.Exceptions;
using Jumping.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Jumping.Services.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly IUserAuthRepository _userAuthRepository;
        private readonly IMapper _mapper;

        public UserService(IUserRepository userRepository, IUserAuthRepository userAuthRepository, IMapper mapper)
        {
            _userRepository = userRepository;
            _userAuthRepository = userAuthRepository;
            _mapper = mapper;
        }

        public async Task<string> AddNewUserAsync(CreateOrUpdateUserDTO request)
        {
            if (request is null)
            {
                throw new AppException($"Request cannot be empty", 400);
            }

            var exsistingSubject = await _userRepository.GetAsync(a => a.UserAuthId == request.UserAuthId);

            if (exsistingSubject?.FirstOrDefault() is not null)
            {
                throw new AppException($"User for {request.UserAuthId} exsists", 400);
            }

            exsistingSubject = await _userRepository.GetAsync(a => a.Id == request.UserAuthId);

            if (exsistingSubject?.FirstOrDefault() is not null)
            {
                throw new AppException($"User for {request.UserAuthId} exsists", 400);
            }

            var exsistingSubject1 = await _userAuthRepository.GetAsync(a => a.Id == request.UserAuthId);

            if (exsistingSubject?.FirstOrDefault() is null)
            {
                throw new AppException($"User for {request.UserAuthId} must be regictered first", 400);
            }

            var createUser = _mapper.Map<User>(request);
            createUser.Id = createUser.UserAuthId;
            createUser.NotificationPreferenceId = createUser.UserAuthId;

            var newId = await _userRepository.CreateAsync(createUser);

            return $"User was successfuly created with id {newId}.";
        }

        public async Task<ICollection<UserResponseDTO>> GetAllUsersAsync()
        {
            var users = await _userRepository.GetAllAsync();

            return _mapper.Map<ICollection<UserResponseDTO>>(users);
        }

        public async Task<UserResponseDTO> GetUserByIdAsync(Guid Id)
        {
            var user = await _userRepository.GetByIdAsync(Id);

            if (user == null)
            {
                throw new AppException($"User with {Id} wasn't found", 500);
            }

            return _mapper.Map<UserResponseDTO>(user);
        }

        // public async Task<string> RemoveUserAsync(Guid id)
        // {
        //     // var user = await _userRepository.GetByIdAsync(id);
        //     //
        //     // if (user == null)
        //     // {
        //         throw new AppException($"User with {id} can not be deleted", 400);
        //     // }
        //     //
        //     // await _userRepository.DeleteAsync(user);
        //     //
        //     return $"User with {id} was succefully removed";
        // }

        public async Task<string> UpdateUserAsync(Guid id, CreateOrUpdateUserDTO userDto)
        {
            if (userDto is null)
            {
                throw new AppException($"Request cannot be empty", 400);
            }

            if (userDto.UserAuthId != id)
            {
                throw new AppException($"User with {id} can not be updated since request inconsistency", 400);
            }

            var user = await _userRepository.GetByIdAsync(id);

            if (user == null)
            {
                throw new AppException($"User with {id} wasn't found", 400);
            }

            if (user.Id != userDto.UserAuthId || user.UserAuthId != userDto.UserAuthId)
            {
                throw new AppException($"User with {id} can not be updated since db inconsistency", 400);
            }

            var updatedUser = _mapper.Map<CreateOrUpdateUserDTO, User>(userDto, user);

            await _userRepository.UpdateAsync(updatedUser);

            return $"User with {id} was succefully updated";
        }
    }
}
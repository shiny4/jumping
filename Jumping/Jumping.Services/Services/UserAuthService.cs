﻿using AutoMapper;
using Jumping.Domain.Abstractions.Repositories;
using Jumping.Domain.Abstractions.Services;
using Jumping.Domain.Common.DTO;
using Jumping.Domain.Common.Exceptions;
using Jumping.Domain.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Jumping.Services.Services
{
    public class UserAuthService : IUserAuthService
    {
        private readonly IUserAuthRepository _userAuthRepository;
        private readonly INotificationPreferenceRepository _notificationPreference;
        private readonly IUserRepository _userRepository;
        private readonly IMapper _mapper;

        public UserAuthService(IUserAuthRepository userAuthRepository,
            INotificationPreferenceRepository notificationPreference,
            IUserRepository userRepository,
            IMapper mapper)
        {
            _userAuthRepository = userAuthRepository;
            _notificationPreference = notificationPreference;
            _userRepository = userRepository;
            _mapper = mapper;
        }

        public async Task<string> AddNewUserAuthAsync(CreateOrUpdateUserAuthDTO request)
        {
            if (request is null)
            {
                throw new AppException($"Request cannot be empty", 400);
            }

            var createUserAuth = _mapper.Map<UserAuth>(request);

            var newId = await _userAuthRepository.CreateAsync(createUserAuth);
            var newId1 = await _notificationPreference.CreateAsync(new NotificationPreference()
                {Id = newId, UserAuthId = newId});
            var newId2 = await _userRepository.CreateAsync(new User()
                {Id = newId, UserAuthId = newId, NotificationPreferenceId = newId1});

            return $"UserAuth was successfuly created with id {newId}.";
        }

        public async Task<ICollection<UserAuthResponseDTO>> GetAllUserAuthsAsync()
        {
            var userAuths = await _userAuthRepository.GetAllAsync();

            return _mapper.Map<ICollection<UserAuthResponseDTO>>(userAuths);
        }

        public async Task<UserAuthResponseDTO> GetUserAuthByIdAsync(Guid Id)
        {
            var userAuth = await _userAuthRepository.GetByIdAsync(Id);

            if (userAuth == null)
            {
                throw new AppException($"UserAuth with {Id} wasn't found", 500);
            }

            return _mapper.Map<UserAuthResponseDTO>(userAuth);
        }

        public async Task<string> RemoveUserAuthAsync(Guid id)
        {
            var userAuth = await _userAuthRepository.GetByIdAsync(id);

            if (userAuth == null)
            {
                throw new AppException($"UserAuth with {id} wasn't found", 500);
            }

            var user = await _userRepository.GetByIdAsync(id);
            if (user != null)
            {
                await _userRepository.DeleteAsync(user);
            }

            var notificationPreference = await _notificationPreference.GetByIdAsync(id);
            if (notificationPreference != null)
            {
                await _notificationPreference.DeleteAsync(notificationPreference);
            }

            await _userAuthRepository.DeleteAsync(userAuth);

            return $"UserAuth with {id} was succefully removed";
        }

        public async Task<string> UpdateUserAuthAsync(Guid id, CreateOrUpdateUserAuthDTO userAuthDto)
        {
            var userAuth = await _userAuthRepository.GetByIdAsync(id);

            if (userAuth == null)
            {
                throw new AppException($"UserAuth with {id} wasn't found", 500);
            }

            var updatedUserAuth = _mapper.Map<CreateOrUpdateUserAuthDTO, UserAuth>(userAuthDto, userAuth);

            await _userAuthRepository.UpdateAsync(updatedUserAuth);

            return $"UserAuth with {id} was succefully updated";
        }
    }
}
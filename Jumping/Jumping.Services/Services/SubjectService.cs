﻿using AutoMapper;
using Jumping.Domain.Abstractions.Repositories;
using Jumping.Domain.Abstractions.Services;
using Jumping.Domain.Common.DTO;
using Jumping.Domain.Common.Exceptions;
using Jumping.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jumping.Services.Services
{
    public class SubjectService : ISubjectService
    {
        private readonly ISubjectRepository _subjectRepository;
        private readonly IMapper _mapper;

        public SubjectService(ISubjectRepository subjectRepository, IMapper mapper)
        {
            _subjectRepository = subjectRepository;
            _mapper = mapper;
        }

        public async Task<string> AddSubjectAsync(CreateOrUpdateSubjectDTO request)
        {
            if (request is null)
            {
                throw new AppException($"Request cannot be empty", 400);
            }

            var exsistingSubject = await _subjectRepository.GetAsync(a => a.Name == request.Name);

            if (exsistingSubject.FirstOrDefault() is not null)
            {
                throw new AppException($"Subject with {request.Name} exsists", 400);
            }

            var newSubject = _mapper.Map<Subject>(request);

            var newId = await _subjectRepository.CreateAsync(newSubject);

            return $"Subject was successfuly created with id {newId}.";
        }

        public async Task<ICollection<SubjectResponseDTO>> GetAllSubjects()
        {
            var subjects = await _subjectRepository.GetAllAsync();

            return _mapper.Map<List<SubjectResponseDTO>>(subjects);
        }

        public async Task<SubjectResponseDTO> GetSubjectByID(Guid id)
        {
            var admin = await _subjectRepository.GetByIdAsync(id);
            return _mapper.Map<SubjectResponseDTO>(admin);
        }

        public async Task RemoveSubjectAsync(Guid id)
        {
            var subject = await _subjectRepository.GetByIdAsync(id);

            if (subject == null)
            {
                throw new AppException($"Subject with {id} wasn't found", 500);
            }

            await _subjectRepository.DeleteAsync(subject);
        }

        public async Task UpdateSubjectAsync(Guid id, CreateOrUpdateSubjectDTO request)
        {
            if (request is null)
            {
                throw new AppException($"Request cannot be empty", 400);
            }

            var exsistingSubject = await _subjectRepository.GetByIdAsync(id);

            if (exsistingSubject is null)
            {
                throw new AppException($"Subject with {id} doesn't exsists", 400);
            }

            var updateSubject = _mapper.Map(request, exsistingSubject);

            await _subjectRepository.UpdateAsync(updateSubject);
        }
    }
}

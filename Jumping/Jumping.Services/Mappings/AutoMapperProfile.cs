﻿using AutoMapper;
using Jumping.Domain.Common.DTO;
using Jumping.Domain.Models;
using Jumping.Domain.Models.Administration;

namespace Jumping.Services.Mappings
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<Admin, AdminResponseDTO>();
            CreateMap<CreateOrUpdateAdminDTO, Admin>();

            // .ForMember(dest => dest.RoleName, opt => opt.MapFrom(src => src.Role.RoleName));
            //CreateMap<User, TeacherResponseDTO>()
            //    .ForMember(dest => dest.RoleName, opt => opt.MapFrom(src => src.Role.RoleName));
            //CreateMap<Student, StudentResponseDTO>()
            //    .ForMember(dest => dest.RoleName, opt => opt.MapFrom(src => src.Role.RoleName));
            //CreateMap<CreateOrUpdateTeacherDTO, User>();
            //CreateMap<CreateOrUpdateStudentDTO, Student>();

            CreateMap<Review, ReviewResponseDTO>();
            CreateMap<CreateOrUpdateReviewDTO, Review>();
            CreateMap<Review, CreateOrUpdateReviewDTO>();


            //CreateMap<SubjectResponseDTO, Subject>();
            CreateMap<Subject, SubjectResponseDTO>();
            CreateMap<CreateOrUpdateSubjectDTO, Subject>();

            CreateMap<Application, ApplicationResponseDTO>();
            CreateMap<CreateOrUpdateApplicationDTO, Application>();

            CreateMap<User, UserResponseDTO>();
            CreateMap<CreateOrUpdateUserDTO, User>();

            CreateMap<UserAuth, UserAuthResponseDTO>();
            CreateMap<CreateOrUpdateUserAuthDTO, UserAuth>();

            CreateMap<SiteMessage, SiteMessageResponseDTO>();
            CreateMap<CreateOrUpdateSiteMessageDTO, SiteMessage>();

            CreateMap<NotificationPreference, NotificationPreferenceResponseDTO>();
            CreateMap<CreateOrUpdateNotificationPreferenceDTO, NotificationPreference>();

            CreateMap<Ticket, TicketResponseDTO>();
            CreateMap<CreateOrUpdateTicketDTO, Ticket>();

            CreateMap<Lesson, LessonResponseDTO>();
            CreateMap<CreateOrUpdateLessonDTO, Lesson>();

            CreateMap<CanceledLesson, CanceledLessonResponseDTO>();
            CreateMap<CreateOrUpdateCanceledLessonDTO, CanceledLesson>();
        }
    }
}
﻿using Jumping.Domain.Models.Administration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jumping.Tests.Builders
{
    public static class AdminBuilder
    {
        public static Admin CreateBaseAdmin()
        {
            return new Admin()
            {
                Id = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8"),
                Email = "someEmail@net",
                FirstName = "Name",
                LastName = "Last Name",
                Password = "fdg",
                PasswordSalt = "df",
                PhoneNumber = "234",
            };
        }
    }
}

﻿using AutoFixture;
using AutoFixture.AutoMoq;
using Jumping.Domain.Abstractions.Services;
using Jumping.Domain.Models.Administration;
using Jumping.Domain.Abstractions.Repositories;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using Jumping.Services.Services;
using Xunit;
using Jumping.Tests.Builders;
using System.Threading.Tasks;
using Jumping.Domain.Common.Exceptions;
using FluentAssertions;
using Jumping.Domain.Common.DTO;
using Jumping.Domain.Enums;
using System.Linq.Expressions;

namespace Jumping.Tests.Services.AdminServices
{
    public class AddNewAdminAsyncTests
    {
        private readonly IAdminService _adminService;
        private readonly IFixture _fixture;
        private readonly Mock<IAdminRepository> _adminRepositoryMock;

        public AddNewAdminAsyncTests()
        {
            _fixture = new Fixture().Customize(new AutoMoqCustomization());
            _adminRepositoryMock = _fixture.Freeze<Mock<IAdminRepository>>();
            _adminService = _fixture.Build<AdminService>().OmitAutoProperties().Create();
        }

        [Fact]
        public async void AddNewAdminAsyncTests_AdminAlreadyExsists_ShouldReturnIsExsistException()
        {
            // Arrange
            var admin = AdminBuilder.CreateBaseAdmin();

            var requestDTO = _fixture
                .Build<CreateOrUpdateAdminDTO>()
                .With(a => a.Email, "someEmail@net")
                .Create();

            // var role = _fixture
            //     .Build<Role>()
            //     .With(a => a.RoleName, RoleEnum.Admin)
            //     .OmitAutoProperties()
            //     .Create();

            _adminRepositoryMock.Setup(repo => repo.GetAsync(repo => repo.Email == requestDTO.Email, It.IsAny<int>()))
                .ReturnsAsync(new List<Admin> {admin});

            // Act
            Func<Task> result = async () => await _adminService.AddNewAdminAsync(requestDTO);

            //// Assert
            await result.Should().ThrowAsync<AppException>()
                .WithMessage($"Exsisting admin with {requestDTO.Email} exsists");
        }
    }
}
﻿using Jumping.DataAccess.Repositories;
using Jumping.Domain.Abstractions.Services;
using Jumping.Services.Services;
using Autofac;

namespace Jumping.Web.Extesions
{
    public class AutofacConfigurationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {

            var serivices = typeof(AdminService).Assembly;
            builder.RegisterType<AdminService>().As<IAdminService>();
            builder.RegisterAssemblyTypes(serivices)
                .Where(t => t.Name.EndsWith("Service"))
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            var repositories = typeof(AdminRepository).Assembly;
            builder.RegisterAssemblyTypes(repositories)
                .Where(t => t.Name.EndsWith("Repository"))
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();
           

            //services.AddScoped<IAdminService, AdminService>();
            //services.AddScoped<IStudentService, StudentService>();
            //services.AddScoped<ITeacherService, TeacherService>();
            //services.AddScoped<IReviewService, ReviewService>();
            ///services.AddScoped<IApplicationService, ApplicationService>();
            ///services.AddScoped<ICanceledLessonService, CanceledLessonService>();
            ///services.AddScoped<ILessonService, LessonService>();
            //services.AddScoped<ISiteMessageService, SiteMessageService>();
            ///services.AddScoped<ITicketService, TicketService>();
            ///services.AddScoped<IUserService, UserService>();
            //services.AddScoped<IUserAuthService, UserAuthService>();
            //services.AddScoped<ISubjectService, SubjectService>();
            //services.AddScoped<INotificationPreferenceService, NotificationPreferenceService>();


            //services.AddScoped<IAdminRepository, AdminRepository>();
            //services.AddScoped<ITeacherRepository, TeacherRepository>();
            //services.AddScoped<IStudentRepository, StudentRepository>();
            //services.AddScoped<IReviewRepository, ReviewRepository>();
            //services.AddScoped<ISubjectRepository, SubjectRepository>();
            //services.AddScoped<IApplicationRepository, ApplicationRepository>();
            //services.AddScoped<ICanceledLessonRepository, CanceledLessonRepository>();
            //services.AddScoped<ILessonRepository, LessonRepository>();
            //services.AddScoped<ISiteMessageRepository, SiteMessageRepository>();
            //services.AddScoped<ITicketRepository, TicketRepository>();
            ///services.AddScoped<IUserRepository, UserRepository>();
            ///services.AddScoped<IUserAuthRepository, UserAuthRepository>();
            //services.AddScoped<INotificationPreferenceRepository, NotificationPreferenceRepository>();

        }
    }    
}
﻿using Jumping.Domain.Abstractions.Services;
using Jumping.Domain.Common.DTO;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Jumping.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserAuthController : ControllerBase
    {
        private readonly IUserAuthService _userAuthService;

        public UserAuthController(IUserAuthService userAuthService)
        {
            _userAuthService = userAuthService;
        }

        [HttpGet]
        public async Task<ActionResult<List<UserAuthResponseDTO>>> GetUserAuthsAsync()
        {
            var admins = await _userAuthService.GetAllUserAuthsAsync();

            return Ok(admins);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<UserAuthResponseDTO>> GetUserAuthAsync(Guid id)
        {
            var result = await _userAuthService.GetUserAuthByIdAsync(id);

            return Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> AddUserAuthAsync(CreateOrUpdateUserAuthDTO request)
        {
            var result = await _userAuthService.AddNewUserAuthAsync(request);

            return Ok(result);
        }

        [HttpPut("{id:guid}")]
        public async Task<IActionResult> UpdateUserAuthAsync(Guid id, CreateOrUpdateUserAuthDTO request)
        {
            var result = await _userAuthService.UpdateUserAuthAsync(id, request);

            return Ok(result);
        }

        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> RemoveUserAuthAsync(Guid id)
        {
            var result = await _userAuthService.RemoveUserAuthAsync(id);

            return Ok(result);
        }
    }
}
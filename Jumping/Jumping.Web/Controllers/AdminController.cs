﻿using Jumping.Domain.Abstractions.Services;
using Jumping.Domain.Common.DTO;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Jumping.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AdminController : ControllerBase
    {
        private readonly IAdminService _adminService;

        public AdminController(IAdminService userService)
        {
            _adminService = userService;
        }

        [HttpGet]
        public async Task<ActionResult<List<AdminResponseDTO>>> GetAdminsAsync()
        {
            var admins = await _adminService.GetAllAdminsAsync();

            return Ok(admins);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<AdminResponseDTO>> GetAdminAsync(Guid id)
        {
            var result = await _adminService.GetAdminByIDAsync(id);

            return Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> AddAdminAsync(CreateOrUpdateAdminDTO request)
        {
            var result = await _adminService.AddNewAdminAsync(request);

            return Ok(result);
        }

        [HttpPut("{id:guid}")]
        public async Task<IActionResult> UpdateAdminAsync(Guid id, CreateOrUpdateAdminDTO request)
        {
            await _adminService.UpdateAdminAsync(id, request);

            return Ok();
        }

        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> RemoveAdminAsync(Guid id)
        {
            await _adminService.RemoveAdminAsync(id);

            return Ok();
        }
    }
}
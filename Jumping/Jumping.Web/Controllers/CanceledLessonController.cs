﻿using Jumping.Domain.Abstractions.Services;
using Jumping.Domain.Common.DTO;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Jumping.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CanceledLessonController : ControllerBase
    {
        private readonly ICanceledLessonService _canceledLessonService;

        public CanceledLessonController(ICanceledLessonService userService)
        {
            _canceledLessonService = userService;
        }

        [HttpGet]
        public async Task<ActionResult<List<CanceledLessonResponseDTO>>> GetCanceledLessonsAsync()
        {
            var admins = await _canceledLessonService.GetAllCanceledLessonsAsync();

            return Ok(admins);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<CanceledLessonResponseDTO>> GetCanceledLessonAsync(Guid id)
        {
            var result = await _canceledLessonService.GetCanceledLessonByIdAsync(id);

            return Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> AddCanceledLessonAsync(CreateOrUpdateCanceledLessonDTO request)
        {
            var result = await _canceledLessonService.AddNewCanceledLessonAsync(request);

            return Ok(result);
        }

        [HttpPut("{id:guid}")]
        public async Task<IActionResult> UpdateCanceledLessonAsync(Guid id, CreateOrUpdateCanceledLessonDTO request)
        {
            var result = await _canceledLessonService.UpdateCanceledLessonAsync(id, request);

            return Ok(result);
        }

        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> RemoveCanceledLessonAsync(Guid id)
        {
            var result = await _canceledLessonService.RemoveCanceledLessonAsync(id);

            return Ok(result);
        }
    }
}
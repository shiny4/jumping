﻿using Jumping.Domain.Abstractions.Services;
using Jumping.Domain.Common.DTO;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Jumping.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LessonController : ControllerBase
    {
        private readonly ILessonService _lessonService;

        public LessonController(ILessonService userService)
        {
            _lessonService = userService;
        }

        [HttpGet]
        public async Task<ActionResult<List<LessonResponseDTO>>> GetLessonsAsync()
        {
            var admins = await _lessonService.GetAllLessonsAsync();

            return Ok(admins);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<LessonResponseDTO>> GetLessonAsync(Guid id)
        {
            var result = await _lessonService.GetLessonByIdAsync(id);

            return Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> AddLessonAsync(CreateOrUpdateLessonDTO request)
        {
            var result = await _lessonService.AddNewLessonAsync(request);

            return Ok(result);
        }

        [HttpPut("{id:guid}")]
        public async Task<IActionResult> UpdateLessonAsync(Guid id, CreateOrUpdateLessonDTO request)
        {
            var result = await _lessonService.UpdateLessonAsync(id, request);

            return Ok(result);
        }

        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> RemoveLessonAsync(Guid id)
        {
            var result = await _lessonService.RemoveLessonAsync(id);

            return Ok(result);
        }
    }
}
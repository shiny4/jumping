﻿using Jumping.Domain.Abstractions.Services;
using Jumping.Domain.Common.DTO;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Jumping.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReviewController : ControllerBase
    {
        private readonly IReviewService _reviewService;

        public ReviewController(IReviewService userService)
        {
            _reviewService = userService;
        }

        [HttpGet]
        public async Task<ActionResult<List<ReviewResponseDTO>>> GetReviewsAsync()
        {
            var admins = await _reviewService.GetAllReviewsAsync();

            return Ok(admins);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<ReviewResponseDTO>> GetReviewAsync(Guid id)
        {
            var result = await _reviewService.GetReviewByIdAsync(id);

            return Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> AddReviewAsync(CreateOrUpdateReviewDTO request)
        {
            var result = await _reviewService.AddNewReviewAsync(request);

            return Ok(result);
        }

        [HttpPut("{id:guid}")]
        public async Task<IActionResult> UpdateReviewAsync(Guid id, CreateOrUpdateReviewDTO request)
        {
            var result = await _reviewService.UpdateReviewAsync(id, request);

            return Ok(result);
        }

        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> RemoveReviewAsync(Guid id)
        {
            var result = await _reviewService.RemoveReviewAsync(id);

            return Ok(result);
        }
    }
}

﻿using Jumping.Domain.Abstractions.Services;
using Jumping.Domain.Common.DTO;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Jumping.Domain.Common.Exceptions;

namespace Jumping.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SubjectController : ControllerBase
    {
        private readonly ISubjectService _subjectService;

        public SubjectController(ISubjectService subjectService)
        {
            _subjectService = subjectService;
        }

        [HttpGet]
        public async Task<ActionResult<List<SubjectResponseDTO>>> GetSubjectsAsync()
        {
            var subjects = await _subjectService.GetAllSubjects();

            return Ok(subjects);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<SubjectResponseDTO>> GetSubjectAsync(Guid id)
        {
            var subject = await _subjectService.GetSubjectByID(id);
            if (subject == null)
            {
                throw new AppException($"Subject with {id} wasn't found", 500);
            }

            return Ok(subject);
        }

        [HttpPost]
        public async Task<ActionResult<SubjectResponseDTO>> AddSubjectAsync(CreateOrUpdateSubjectDTO request)
        {
            var subject = await _subjectService.AddSubjectAsync(request);

            return Ok(subject);
        }

        [HttpPut("{id:guid}")]
        public async Task<IActionResult> UpdateSubjectAsync(Guid id, CreateOrUpdateSubjectDTO request)
        {
            await _subjectService.UpdateSubjectAsync(id, request);

            return Ok();
        }

        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> RemoveSubjectAsync(Guid id)
        {
            await _subjectService.RemoveSubjectAsync(id);

            return Ok();
        }
    }
}
﻿using Jumping.Domain.Abstractions.Services;
using Jumping.Domain.Common.DTO;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Jumping.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SiteMessageController : ControllerBase
    {
        private readonly ISiteMessageService _siteMessageService;

        public SiteMessageController(ISiteMessageService userService)
        {
            _siteMessageService = userService;
        }

        [HttpGet]
        public async Task<ActionResult<List<SiteMessageResponseDTO>>> GetSiteMessagesAsync()
        {
            var admins = await _siteMessageService.GetAllSiteMessagesAsync();

            return Ok(admins);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<SiteMessageResponseDTO>> GetSiteMessageAsync(Guid id)
        {
            var result = await _siteMessageService.GetSiteMessageByIdAsync(id);

            return Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> AddSiteMessageAsync(CreateOrUpdateSiteMessageDTO request)
        {
            var result = await _siteMessageService.AddNewSiteMessageAsync(request);

            return Ok(result);
        }

        [HttpPut("{id:guid}")]
        public async Task<IActionResult> UpdateSiteMessageAsync(Guid id, CreateOrUpdateSiteMessageDTO request)
        {
            var result = await _siteMessageService.UpdateSiteMessageAsync(id, request);

            return Ok(result);
        }

        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> RemoveSiteMessageAsync(Guid id)
        {
            var result = await _siteMessageService.RemoveSiteMessageAsync(id);

            return Ok(result);
        }
    }
}
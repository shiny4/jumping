﻿using Jumping.Domain.Abstractions.Services;
using Jumping.Domain.Common.DTO;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Jumping.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TicketController : ControllerBase
    {
        private readonly ITicketService _ticketService;

        public TicketController(ITicketService userService)
        {
            _ticketService = userService;
        }

        [HttpGet]
        public async Task<ActionResult<List<TicketResponseDTO>>> GetTicketsAsync()
        {
            var admins = await _ticketService.GetAllTicketsAsync();

            return Ok(admins);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<TicketResponseDTO>> GetTicketAsync(Guid id)
        {
            var result = await _ticketService.GetTicketByIdAsync(id);

            return Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> AddTicketAsync(CreateOrUpdateTicketDTO request)
        {
            var result = await _ticketService.AddNewTicketAsync(request);

            return Ok(result);
        }

        [HttpPut("{id:guid}")]
        public async Task<IActionResult> UpdateTicketAsync(Guid id, CreateOrUpdateTicketDTO request)
        {
            var result = await _ticketService.UpdateTicketAsync(id, request);

            return Ok(result);
        }

        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> RemoveTicketAsync(Guid id)
        {
            var result = await _ticketService.RemoveTicketAsync(id);

            return Ok(result);
        }
    }
}
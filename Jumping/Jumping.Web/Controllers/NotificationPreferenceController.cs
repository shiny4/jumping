﻿using Jumping.Domain.Abstractions.Services;
using Jumping.Domain.Common.DTO;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Jumping.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NotificationPreferenceController : ControllerBase
    {
        private readonly INotificationPreferenceService _notificationPreferenceService;

        public NotificationPreferenceController(INotificationPreferenceService notificationPreferenceService)
        {
            _notificationPreferenceService = notificationPreferenceService;
        }

        [HttpGet]
        public async Task<ActionResult<List<NotificationPreferenceResponseDTO>>> GetNotificationPreferencesAsync()
        {
            var admins = await _notificationPreferenceService.GetAllNotificationPreferencesAsync();

            return Ok(admins);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<NotificationPreferenceResponseDTO>> GetNotificationPreferenceAsync(Guid id)
        {
            var result = await _notificationPreferenceService.GetNotificationPreferenceByIdAsync(id);

            return Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> AddNotificationPreferenceAsync(CreateOrUpdateNotificationPreferenceDTO request)
        {
            var result = await _notificationPreferenceService.AddNewNotificationPreferenceAsync(request);

            return Ok(result);
        }

        [HttpPut("{id:guid}")]
        public async Task<IActionResult> UpdateNotificationPreferenceAsync(Guid id,
            CreateOrUpdateNotificationPreferenceDTO request)
        {
            var result = await _notificationPreferenceService.UpdateNotificationPreferenceAsync(id, request);

            return Ok(result);
        }

        // [HttpDelete("{id:guid}")]
        // public async Task<IActionResult> RemoveNotificationPreferenceAsync(Guid id)
        // {
        //     var result = await _notificationPreferenceService.RemoveNotificationPreferenceAsync(id);
        //
        //     return Ok(result);
        // }
    }
}
﻿using Jumping.Domain.Abstractions.Services;
using Jumping.Domain.Common.DTO;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Jumping.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpGet]
        public async Task<ActionResult<List<UserResponseDTO>>> GetUsersAsync()
        {
            var admins = await _userService.GetAllUsersAsync();

            return Ok(admins);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<UserResponseDTO>> GetUserAsync(Guid id)
        {
            var result = await _userService.GetUserByIdAsync(id);

            return Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> AddUserAsync(CreateOrUpdateUserDTO request)
        {
            var result = await _userService.AddNewUserAsync(request);

            return Ok(result);
        }

        [HttpPut("{id:guid}")]
        public async Task<IActionResult> UpdateUserAsync(Guid id, CreateOrUpdateUserDTO request)
        {
            var result = await _userService.UpdateUserAsync(id, request);

            return Ok(result);
        }

        // [HttpDelete("{id:guid}")]
        // public async Task<IActionResult> RemoveUserAsync(Guid id)
        // {
        //     var result = await _userService.RemoveUserAsync(id);
        //
        //     return Ok(result);
        // }
    }
}
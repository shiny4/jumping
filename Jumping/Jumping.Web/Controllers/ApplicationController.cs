﻿using Jumping.Domain.Abstractions.Services;
using Jumping.Domain.Common.DTO;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Jumping.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ApplicationController : ControllerBase
    {
        private readonly IApplicationService _applicationService;

        public ApplicationController(IApplicationService applicationService)
        {
            _applicationService = applicationService;
        }

        [HttpGet]
        public async Task<ActionResult<List<ApplicationResponseDTO>>> GetApplicationsAsync()
        {
            var applications = await _applicationService.GetAllApplications();

            return Ok(applications);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<ApplicationResponseDTO>> GetApplicationAsync(Guid id)
        {
            var application = await _applicationService.GetApplicationByID(id);

            return Ok(application);
        }

        [HttpPost]
        public async Task<ActionResult<ApplicationResponseDTO>> AddApplicationAsync(CreateOrUpdateApplicationDTO request)
        {
            var application = await _applicationService.AddApplicationAsync(request);

            return Ok(application);
        }

        [HttpPut("{id:guid}")]
        public async Task<IActionResult> UpdateApplicationAsync(Guid id, CreateOrUpdateApplicationDTO request)
        {
            await _applicationService.UpdateApplicationAsync(id, request);

            return Ok();
        }

        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> RemoveApplicationAsync(Guid id)
        {
            await _applicationService.RemoveApplicationAsync(id);

            return Ok();
        }
    }
}

using Jumping.Web.Extesions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using AutoMapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Jumping.DataAccess;
using Microsoft.EntityFrameworkCore;
using Jumping.Services.Mappings;
using Jumping.Web.Infrastructure.Middlewares;
using Autofac;
using Autofac.Extensions.DependencyInjection;

namespace Jumping.Web
{
    public class Startup
    {
        public ILifetimeScope AutofacContainer { get; private set; }
        public IConfiguration Configuration { get; private set; }
        public Startup(IConfiguration configuration)
        {
            this.Configuration = configuration;
        }

        public void ConfigureContainer(ContainerBuilder builder){
            builder.RegisterModule(new AutofacConfigurationModule());
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddOptions();

            services.AddControllersWithViews().AddNewtonsoftJson(options =>
                options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);

            services.AddControllers().AddMvcOptions(x =>
                x.SuppressAsyncSuffixInActionNames = false);

            services.AddAutoMapper(typeof(AutoMapperProfile));           

            services.AddDbContext<DataContext>(options =>
            {
                // options.UseSqlServer(Configuration.GetConnectionString("SqlConnection"));
                options.UseNpgsql(Configuration.GetConnectionString("PostgreConnection"));
                //options.UseSnakeCaseNamingConvention();
                //options.UseLazyLoadingProxies();
            });
            services.AddScoped<IDatabaseInitializer, DatabaseInitializer>();
            services.AddOpenApiDocument(options =>
            {
                options.Title = "PromoCode Factory API Doc";
                options.Version = "1.0";
            });


        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, DataContext dataContext,
            IDatabaseInitializer dbInitializer)
        {
            this.AutofacContainer = app.ApplicationServices.GetAutofacRoot();

            if (env.IsDevelopment())
            {
                //  app.UseDeveloperExceptionPage();
                app.UseExceptionHandling();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }


            app.UseOpenApi();
            app.UseSwaggerUi3(x => { x.DocExpansion = "list"; });

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            //     app.UseSpaStaticFiles();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller}/{action=Index}/{id?}");
            });

            if (true)
            {
                dbInitializer.Initialize();
            }

            //if (true)  DatabaseInitializer.Initialize(dataContext);
        }
    }
}
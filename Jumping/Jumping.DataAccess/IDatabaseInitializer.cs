namespace Jumping.DataAccess
{
    public interface IDatabaseInitializer
    {
        public void Initialize(DataContext dbContext);

        public void Initialize();
    }
}
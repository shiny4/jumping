﻿using Jumping.Domain;
using Jumping.Domain.Abstractions.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Jumping.DataAccess.Repositories
{
    public abstract class BaseRepository<TEntity> : IBaseRepository<TEntity>
        where TEntity : BaseEntity
    {
        protected DbContext Context { get; }

        protected DbSet<TEntity> DbSet { get; }

        public BaseRepository(DbContext context)
        {
            Context = context;
            DbSet = Context?.Set<TEntity>();
        }

        public async Task<ICollection<TEntity>> GetAllAsync()
        {
            return await DbSet.AsNoTracking().ToListAsync();
        }

        /// <summary>
        /// Collection only for read
        /// </summary>
        /// <param name="predicate"></param>
        /// <param name="maxItemsAmount"></param>
        /// <returns></returns>
        public async Task<ICollection<TEntity>> GetAsync(Expression<Func<TEntity, bool>> predicate,
            int maxItemsAmount = 0)
        {
            var query = DbSet.AsNoTracking().Where(predicate);
            query = maxItemsAmount > 0 ? query.Take(maxItemsAmount) : query;

            return await query.ToListAsync();
        }

        public async Task<TEntity> GetByIdAsync(Guid id)
        {
            return await DbSet.SingleOrDefaultAsync(e => e.Id == id);
        }

        public async Task<Guid> CreateAsync(TEntity entity)
        {
            await DbSet.AddAsync(entity);

            await SaveChanges();

            return entity.Id;
        }

        public async Task CreateBulkAsync(ICollection<TEntity> entities)
        {
            await DbSet.AddRangeAsync(entities);

            await SaveChanges();
        }

        public async Task UpdateAsync(TEntity item)
        {
            await SaveChanges();
        }

        public async Task DeleteAsync(TEntity item)
        {
            DbSet.Remove(item);

            await SaveChanges();
        }

        public async Task DeleteBulkAsync(ICollection<TEntity> items)
        {
            DbSet.RemoveRange(items);

            await SaveChanges();
        }

        private async Task SaveChanges()
        {
            await Context.SaveChangesAsync();
        }
    }
}
using Jumping.Domain.Abstractions.Repositories;
using Jumping.Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace Jumping.DataAccess.Repositories
{
    public class CanceledLessonRepository : BaseRepository<CanceledLesson>, ICanceledLessonRepository
    {
        public CanceledLessonRepository(DataContext context) : base(context)
        {
        }
    }
}
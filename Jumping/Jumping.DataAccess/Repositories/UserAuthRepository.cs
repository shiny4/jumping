﻿using Jumping.Domain.Abstractions.Repositories;
using Jumping.Domain.Models;

namespace Jumping.DataAccess.Repositories
{
    public class UserAuthRepository : BaseRepository<UserAuth>, IUserAuthRepository
    {
        public UserAuthRepository(DataContext context) : base(context)
        {
        }
    }
}
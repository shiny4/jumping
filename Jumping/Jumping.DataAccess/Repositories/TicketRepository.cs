using Jumping.Domain.Abstractions.Repositories;
using Jumping.Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace Jumping.DataAccess.Repositories
{
    public class TicketRepository : BaseRepository<Ticket>, ITicketRepository
    {
        public TicketRepository(DataContext context) : base(context)
        {
        }
    }
}
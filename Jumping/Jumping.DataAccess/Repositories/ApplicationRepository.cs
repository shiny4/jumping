﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Jumping.Domain.Abstractions.Repositories;
using Jumping.Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace Jumping.DataAccess.Repositories
{
    public class ApplicationRepository : BaseRepository<Application>, IApplicationRepository
    {
        public ApplicationRepository(DataContext context) : base(context)
        {
        }

        public async Task<ICollection<Application>> GetAllApplicationsWithSubjectsTeachersStudentsAsync()
        {
            return await DbSet.AsNoTracking()
                .Include(r => r.Student)
                .Include(t => t.Teacher)
                .Include(s => s.Subject)
                .ToListAsync();
        }

        public async Task<Application> GetApplicationForUpdateAsync(Guid id)
        {
            return await DbSet.Include(s => s.Student)
                .Include(t => t.Teacher)
                .Include(s => s.Subject)
                .FirstOrDefaultAsync(r => r.Id == id);
        }

        public async Task<Application> GetApplicationWithSubjectTeacherStudentByIdAsync(Guid id)
        {
            return await DbSet.AsNoTracking()
                .Include(r => r.Student)
                //.ThenInclude(r => r.Role)
                .Include(t => t.Teacher)
                //.ThenInclude(r => r.Role)
                .Include(s => s.Subject)
                .FirstOrDefaultAsync(a => a.Id == id);
        }
    }
}
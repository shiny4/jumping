﻿using Jumping.Domain.Abstractions.Repositories;
using Jumping.Domain.Models;

namespace Jumping.DataAccess.Repositories
{
    public class UserRepository : BaseRepository<User>, IUserRepository
    {
        public UserRepository(DataContext context) : base(context)
        {
        }
    }
}
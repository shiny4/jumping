﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Jumping.Domain.Abstractions.Repositories;
using Jumping.Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace Jumping.DataAccess.Repositories
{
    public class SubjectRepository : BaseRepository<Subject>, ISubjectRepository
    {
        public SubjectRepository(DataContext context) : base(context)
        {
        }

        public async Task<ICollection<Subject>> GetAllSubjectsWithCategoriesAsync()
        {
            return await DbSet.AsNoTracking()
                              .Include(c => c.Category)
                              .ToListAsync();
        }

        public async Task<Subject> GetSubjectForUpdateAsync(Guid id)
        {
            return await DbSet.Include(c => c.Category)
                              .FirstOrDefaultAsync(r => r.Id == id);
        }

        public async Task<Subject> GetSubjectWithCategoryByIdAsync(Guid id)
        {
            return await DbSet.AsNoTracking()
                              .Include(c => c.Category)
                              .FirstOrDefaultAsync(a => a.Id == id);
        }
    }
}

﻿using Jumping.Domain.Abstractions.Repositories;
using Jumping.Domain.Models.Administration;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Jumping.Domain.Models;

namespace Jumping.DataAccess.Repositories
{
    public class AdminRepository : BaseRepository<Admin>, IAdminRepository
    {
        public AdminRepository(DataContext context) : base(context)
        {
        }

        public async Task<Admin> GetAdminForUpdateAsync(Guid id)
        {
            return await DbSet.FirstOrDefaultAsync(a => a.Id == id);
        }

        /// <summary>
        /// Collection only for read
        /// </summary>
        /// <returns></returns>
        public async Task<ICollection<Admin>> GetAdminsAsync()
        {
            return await DbSet.AsNoTracking().ToListAsync();
        }

        /// <summary>
        /// Admin only for read
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Admin> GetAdminAsync(Guid id)
        {
            return await DbSet.AsNoTracking().FirstOrDefaultAsync(a => a.Id == id);
        }
    }
}
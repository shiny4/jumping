﻿using Jumping.Domain.Abstractions.Repositories;
using Jumping.Domain.Models;

namespace Jumping.DataAccess.Repositories
{
    public class NotificationPreferenceRepository : BaseRepository<NotificationPreference>,
        INotificationPreferenceRepository
    {
        public NotificationPreferenceRepository(DataContext context) : base(context)
        {
        }
    }
}
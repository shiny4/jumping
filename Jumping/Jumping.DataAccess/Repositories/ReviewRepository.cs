﻿using Jumping.Domain.Abstractions.Repositories;
using Jumping.Domain.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jumping.DataAccess.Repositories
{
    public class ReviewRepository : BaseRepository<Review>, IReviewRepository
    {
        public ReviewRepository(DataContext context) : base(context)
        {
        }

        public async Task<ICollection<Review>> GetAllReviewsWithTeachersStudentsAsync()
        {
            return await DbSet.AsNoTracking()
                .Include(r => r.OnWhom)
                //.ThenInclude(r => r.Role)
                .Include(t => t.Auther)
                // .ThenInclude(r => r.Role)
                .ToListAsync();
        }

        public async Task<Review> GetReviewForUpdateAsync(Guid id)
        {
            return await DbSet.Include(r => r.OnWhom)
                .Include(t => t.Auther)
                .FirstOrDefaultAsync(r => r.Id == id);
        }

        public async Task<Review> GetReviewWithTeacherStudentByIdAsync(Guid id)
        {
            return await DbSet.AsNoTracking()
                .Include(r => r.OnWhom)
                //.ThenInclude(r => r.Role)
                .Include(t => t.Auther)
                //.ThenInclude(r => r.Role)
                .FirstOrDefaultAsync(a => a.Id == id);
        }
    }
}
using Jumping.Domain.Abstractions.Repositories;
using Jumping.Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace Jumping.DataAccess.Repositories
{
    public class SiteMessageRepository : BaseRepository<SiteMessage>, ISiteMessageRepository
    {
        public SiteMessageRepository(DataContext context) : base(context)
        {
        }
    }
}
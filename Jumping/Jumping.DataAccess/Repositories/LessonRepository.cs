﻿using Jumping.Domain.Abstractions.Repositories;
using Jumping.Domain.Models;

namespace Jumping.DataAccess.Repositories
{
    public class LessonRepository : BaseRepository<Lesson>, ILessonRepository
    {
        public LessonRepository(DataContext context) : base(context)
        {
        }
    }
}

﻿using System;
using Jumping.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Jumping.DataAccess.EntityConfigurations
{
    public class ApplicationEntityConfiguration : IEntityTypeConfiguration<Application>
    {
        public void Configure(EntityTypeBuilder<Application> builder)
        {
            builder.ToTable("Applications");

            builder.HasKey(r => r.Id);

            //builder.Property(r => r.Subject).IsRequired().HasMaxLength(100);
            builder.Property(r => r.Description).IsRequired().HasMaxLength(200);
            builder.Property(r => r.StudentId).IsRequired();
            builder.Property(r => r.TeacherId).IsRequired();
            builder.Property(r => r.CreationDate).IsRequired().HasDefaultValue(DateTime.Now);
            builder.Property(r => r.DesiredDateTime).IsRequired();
            builder.Property(r => r.ActuallUntil).IsRequired().HasDefaultValue(DateTime.Now.AddDays(3));


            builder
                .HasOne<Subject>(s => s.Subject)
                .WithMany(g => g.Applications)
                .HasForeignKey(s => s.SubjectId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
﻿using Jumping.Domain.Models.Administration;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jumping.DataAccess.EntityConfigurations
{
    public class AdminEntityConfiguration : IEntityTypeConfiguration<Admin>
    {
        public void Configure(EntityTypeBuilder<Admin> builder)
        {
            builder.ToTable("Admins");

            builder.HasKey(r => r.Id);

            builder.Property(r => r.FirstName).IsRequired().HasMaxLength(100);
            builder.Property(r => r.LastName).IsRequired().HasMaxLength(100);
            builder.Property(r => r.PhoneNumber).IsRequired().HasMaxLength(100);
            builder.Property(r => r.Password).IsRequired().HasMaxLength(100);
            builder.Property(r => r.PasswordSalt).IsRequired().HasMaxLength(100);
            builder.Property(r => r.Email).IsRequired().HasMaxLength(100);
        }
    }
}
﻿using Jumping.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jumping.DataAccess.EntityConfigurations
{
    public class ReviewEntityConfiguration : IEntityTypeConfiguration<Review>
    {
        public void Configure(EntityTypeBuilder<Review> builder)
        {
            builder.ToTable("Reviews");

            builder.HasKey(r => r.Id);

            builder.Property(r => r.Description).IsRequired().HasMaxLength(500);
            builder.Property(r => r.Score).HasDefaultValue(0);
            builder.Property(r => r.AutherId).IsRequired();
            builder.Property(r => r.LessonId).IsRequired();
            builder.Property(r => r.OnWhomId).IsRequired();

            builder
                .HasOne<Lesson>(s => s.Lesson)
                .WithMany(g => g.Reviews)
                .HasForeignKey(s => s.LessonId)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
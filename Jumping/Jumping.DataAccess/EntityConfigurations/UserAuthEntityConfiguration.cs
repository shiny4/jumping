using Jumping.Domain.Enums;
using Jumping.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Jumping.DataAccess.EntityConfigurations
{
    public class UserAuthEntityConfiguration : IEntityTypeConfiguration<UserAuth>
    {
        public void Configure(EntityTypeBuilder<UserAuth> builder)
        {
            builder.ToTable("UserAuths");
            builder.Property(r => r.Login).IsRequired();
            builder.Property(r => r.Password).IsRequired();
            builder.Property(r => r.PasswordSalt).IsRequired();
            builder.Property(r => r.Status).HasDefaultValue(UserStatusEnum.Active);

            // builder
            //     .HasOne<NotificationPreference>(s => s.notificationPreference)
            //     .WithOne(g => g.UserAuth)
            //     .HasForeignKey<NotificationPreference>(s => s.UserAuthId)
            //     .OnDelete(DeleteBehavior.Cascade);

            builder
                .HasOne<NotificationPreference>(s => s.NotificationPreference)
                .WithOne(g => g.UserAuth)
                .HasForeignKey<NotificationPreference>(s => s.UserAuthId)
                .OnDelete(DeleteBehavior.Cascade);

            builder
                .HasOne<User>(s => s.User)
                .WithOne(g => g.UserAuth)
                .HasForeignKey<User>(s => s.UserAuthId)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
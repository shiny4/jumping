﻿using Jumping.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Jumping.Domain.Enums;

namespace Jumping.DataAccess.EntityConfigurations
{
    public class SubjectEntityConfiguration : IEntityTypeConfiguration<Subject>
    {
        public void Configure(EntityTypeBuilder<Subject> builder)
        {
            builder.ToTable("Subjects");

            builder.HasKey(r => r.Id);

            builder.Property(r => r.Name).IsRequired();
            builder.Property(r => r.Category).HasDefaultValue(Category.CategoryEnum.None);


            builder
                .HasMany(c => c.Users)
                .WithMany(s => s.Subjects)
                .UsingEntity(j => j.ToTable("SubjectUser"));
        }
    }
}
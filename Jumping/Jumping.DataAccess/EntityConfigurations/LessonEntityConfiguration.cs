﻿using Jumping.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Jumping.Domain.Enums;

namespace Jumping.DataAccess.EntityConfigurations
{
    public class LessonEntityConfiguration : IEntityTypeConfiguration<Lesson>
    {
        public void Configure(EntityTypeBuilder<Lesson> builder)
        {
            builder.ToTable("Lessons");

            builder.HasKey(r => r.Id);

            builder.Property(r => r.StartDate).IsRequired();
            builder.Property(r => r.LessonDescription).IsRequired(false).HasMaxLength(400);
            builder.Property(r => r.Status).HasDefaultValue(LessonStatus.OpenedForRecord);
            builder.Property(r => r.DurationInMin).IsRequired();
            builder.Property(r => r.IsIndividual).IsRequired().HasDefaultValue(true);
            builder.Property(r => r.SubjectId).IsRequired();
            builder.Property(r => r.TeacherId).IsRequired();


            builder
                .HasOne<Subject>(s => s.Subject)
                .WithMany(g => g.Lessons)
                .HasForeignKey(s => s.SubjectId)
                .OnDelete(DeleteBehavior.Restrict);

            builder
                .HasOne<User>(s => s.Teacher)
                .WithMany(g => g.LessonsAsTeacher)
                .HasForeignKey(s => s.TeacherId)
                .OnDelete(DeleteBehavior.SetNull);

            builder
                .HasOne<CanceledLesson>(s => s.СanceledLesson)
                .WithOne(g => g.Lesson)
                .HasForeignKey<CanceledLesson>(s => s.LessonId)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
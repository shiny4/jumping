using Jumping.Domain.Enums;
using Jumping.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Jumping.DataAccess.EntityConfigurations
{
    public class NotificationPreferenceEntityConfiguration : IEntityTypeConfiguration<NotificationPreference>
    {
        public void Configure(EntityTypeBuilder<NotificationPreference> builder)
        {
            builder.ToTable("NotificationPreferences");

            //builder.Property(r => r.UserId).IsRequired();
            builder.Property(r => r.NoteAboutAnswer).HasDefaultValue(true);
            builder.Property(r => r.NoteAboutMessage).HasDefaultValue(true);
            builder.Property(r => r.NoteAboutLesson).HasDefaultValue(NotificationEnum.Befor30Min);
        }
    }
}
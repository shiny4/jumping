using Jumping.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Jumping.DataAccess.EntityConfigurations
{
    public class SiteMessageEntityConfiguration : IEntityTypeConfiguration<SiteMessage>
    {
        public void Configure(EntityTypeBuilder<SiteMessage> builder)
        {
            builder.ToTable("SiteMessages");
            builder.Property(r => r.Content).IsRequired();
            builder.Property(r => r.Theme).IsRequired();

            builder
                .HasOne<SiteMessage>(s => s.PreviosMessage)
                .WithMany(g => g.MessageTree)
                .HasForeignKey(s => s.PreviosMessageId)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
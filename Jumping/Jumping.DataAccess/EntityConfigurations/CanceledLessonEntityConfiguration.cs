using Jumping.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Jumping.DataAccess.EntityConfigurations
{
    public class CanceledLessonEntityConfiguration : IEntityTypeConfiguration<CanceledLesson>
    {
        public void Configure(EntityTypeBuilder<CanceledLesson> builder)
        {
            builder.ToTable("CanceledLessons");

            builder.Property(r => r.Role).IsRequired();
            builder.Property(r => r.CancelDate).IsRequired();
            builder.Property(r => r.LessonId).IsRequired();

            builder
                .HasOne<User>(s => s.Initiator)
                .WithMany(g => g.CanceledLessons)
                .HasForeignKey(s => s.InitiatorId)
                .OnDelete(DeleteBehavior.SetNull);
        }
    }
}
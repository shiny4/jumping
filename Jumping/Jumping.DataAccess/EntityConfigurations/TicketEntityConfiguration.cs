using Jumping.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Jumping.DataAccess.EntityConfigurations
{
    public class TicketEntityConfiguration : IEntityTypeConfiguration<Ticket>
    {
        public void Configure(EntityTypeBuilder<Ticket> builder)
        {
            builder.ToTable("Tickets");

            builder.Property(r => r.FromUserId).IsRequired();
            builder.Property(r => r.Theme).IsRequired();
            builder.Property(r => r.Content).IsRequired();


            builder
                .HasOne<User>(s => s.FromUser)
                .WithMany(g => g.Tickets)
                .HasForeignKey(s => s.FromUserId)
                .OnDelete(DeleteBehavior.SetNull);

            builder
                .HasOne<Ticket>(s => s.PreviosTicket)
                .WithMany(g => g.TicketTree)
                .HasForeignKey(s => s.PreviosTicketId)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
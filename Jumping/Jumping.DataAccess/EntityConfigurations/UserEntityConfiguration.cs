﻿using Jumping.Domain.Models;
using Jumping.Domain.Models.Administration;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jumping.DataAccess.EntityConfigurations
{
    public class UserEntityConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.ToTable("Users");

            builder.HasKey(r => r.Id);

            builder.Property(r => r.FirstName).HasMaxLength(100);
            builder.Property(r => r.LastName).HasMaxLength(100);
            builder.Property(r => r.PhoneNumber).HasMaxLength(100);
            builder.Property(r => r.Email).HasMaxLength(100);
            //builder.Property(r => r.UserAuth).IsRequired();
            builder.Property(r => r.RatingAsTeacher).HasDefaultValue(0);
            builder.Property(r => r.RatingAsUser).HasDefaultValue(0);


            builder
                .HasMany<Review>(s => s.ReviewsByMe)
                .WithOne(g => g.Auther)
                .HasForeignKey(s => s.AutherId)
                .OnDelete(DeleteBehavior.SetNull);

            builder
                .HasMany<Review>(s => s.ReviewsOnMe)
                .WithOne(g => g.OnWhom)
                .HasForeignKey(s => s.OnWhomId)
                .OnDelete(DeleteBehavior.Cascade);

            builder
                .HasMany<SiteMessage>(s => s.InSiteMessages)
                .WithOne(g => g.ToUser)
                .HasForeignKey(s => s.ToUserId)
                .OnDelete(DeleteBehavior.Cascade);

            builder
                .HasMany<SiteMessage>(s => s.OutSiteMessages)
                .WithOne(g => g.FromUser)
                .HasForeignKey(s => s.FromUserId)
                .OnDelete(DeleteBehavior.Cascade);


            builder
                .HasMany<Application>(s => s.InApplications)
                .WithOne(g => g.Teacher)
                .HasForeignKey(s => s.TeacherId)
                .OnDelete(DeleteBehavior.Cascade);

            builder
                .HasMany<Application>(s => s.OutApplications)
                .WithOne(g => g.Student)
                .HasForeignKey(s => s.StudentId)
                .OnDelete(DeleteBehavior.Cascade);

            builder
                .HasMany(c => c.LessonsAsStudent)
                .WithMany(s => s.Students)
                .UsingEntity(j => j.ToTable("LessonStudent"));
        }
    }
}
﻿using Jumping.DataAccess.EntityConfigurations;
using Jumping.Domain.Models;
using Jumping.Domain.Models.Administration;
using Microsoft.EntityFrameworkCore;

namespace Jumping.DataAccess
{
    public class DataContext : DbContext
    {
        public DbSet<Admin> Admins { get; set; }

        public DbSet<Lesson> Lessons { get; set; }

        public DbSet<Subject> Subjects { get; set; }

        public DbSet<Review> Reviews { get; set; }

        public DbSet<Application> Applications { get; set; }

        public DbSet<User> Users { get; set; }

        public DbSet<CanceledLesson> CanceledLessons { get; set; }

        public DbSet<NotificationPreference> NotificationPreferences { get; set; }

        public DbSet<SiteMessage> SiteMessages { get; set; }

        public DbSet<Ticket> Tickets { get; set; }

        public DbSet<UserAuth> UserAuths { get; set; }

        public DataContext()
        {
        }

        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new AdminEntityConfiguration());
            modelBuilder.ApplyConfiguration(new ApplicationEntityConfiguration());
            modelBuilder.ApplyConfiguration(new LessonEntityConfiguration());
            modelBuilder.ApplyConfiguration(new UserEntityConfiguration());
            modelBuilder.ApplyConfiguration(new ReviewEntityConfiguration());
            modelBuilder.ApplyConfiguration(new SubjectEntityConfiguration());

            modelBuilder.ApplyConfiguration(new CanceledLessonEntityConfiguration());
            modelBuilder.ApplyConfiguration(new NotificationPreferenceEntityConfiguration());
            modelBuilder.ApplyConfiguration(new SiteMessageEntityConfiguration());
            modelBuilder.ApplyConfiguration(new TicketEntityConfiguration());
            modelBuilder.ApplyConfiguration(new UserAuthEntityConfiguration());

            base.OnModelCreating(modelBuilder);
        }
    }
}
﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Jumping.DataAccess.Migrations
{
    public partial class ChangeApplicationEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SubjectName",
                table: "Application");

            migrationBuilder.AddColumn<Guid>(
                name: "SubjectId",
                table: "Application",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "TeacherId",
                table: "Application",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateIndex(
                name: "IX_Application_SubjectId",
                table: "Application",
                column: "SubjectId");

            migrationBuilder.CreateIndex(
                name: "IX_Application_TeacherId",
                table: "Application",
                column: "TeacherId");

            migrationBuilder.AddForeignKey(
                name: "FK_Application_Subject_SubjectId",
                table: "Application",
                column: "SubjectId",
                principalTable: "Subject",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Application_Teacher_TeacherId",
                table: "Application",
                column: "TeacherId",
                principalTable: "Teacher",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Application_Subject_SubjectId",
                table: "Application");

            migrationBuilder.DropForeignKey(
                name: "FK_Application_Teacher_TeacherId",
                table: "Application");

            migrationBuilder.DropIndex(
                name: "IX_Application_SubjectId",
                table: "Application");

            migrationBuilder.DropIndex(
                name: "IX_Application_TeacherId",
                table: "Application");

            migrationBuilder.DropColumn(
                name: "SubjectId",
                table: "Application");

            migrationBuilder.DropColumn(
                name: "TeacherId",
                table: "Application");

            migrationBuilder.AddColumn<string>(
                name: "SubjectName",
                table: "Application",
                type: "character varying(100)",
                maxLength: 100,
                nullable: false,
                defaultValue: "");
        }
    }
}

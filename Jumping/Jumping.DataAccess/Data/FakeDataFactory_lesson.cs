using System;
using System.Collections.Generic;
using Jumping.Domain.Enums;
using Jumping.Domain.Models;

namespace Jumping.DataAccess.Data
{
    public partial class FakeDataFactory
    {
        public static List<Lesson> Lessons
        {
            get
            {
                var userlist = AuthUsers;
                var result = new List<Lesson>()
                {
                    new Lesson()
                    {
                        Id = Guid.Parse("5a3a7b57-3f09-4763-a375-b9f7c1699fd9"),
                        LessonDescription = "New topic 1",
                        StartDate = DateTime.UtcNow.AddDays(5),
                        DurationInMin = 90,
                        IsIndividual = false,
                        Status = LessonStatus.ClosedForRecord,
                        Price = 100,
                        TeacherId = Guid.Parse("2c73101f-d2d4-46e4-9e29-93459dc71bc7"),
                        SubjectId = Guid.Parse("991533d5-d8d5-4a14-9c7b-eb9f14e1a111"),
                        // Students = new List<User>
                        // {
                        //     userlist[3].User,
                        //     userlist[4].User
                        // }
                    },
                    new Lesson()
                    {
                        Id = Guid.Parse("5b94ba37-52c3-4ffd-9968-795c60e9b0ba"),
                        LessonDescription = "New topic 2",
                        StartDate = DateTime.UtcNow.AddDays(1),
                        DurationInMin = 60,
                        IsIndividual = false,
                        Status = LessonStatus.OpenedForRecord,
                        Price = 200,
                        TeacherId = Guid.Parse("2c73101f-d2d4-46e4-9e29-93459dc71bc7"),
                        SubjectId = Guid.Parse("991533d5-d8d5-4a14-9c7b-eb9f14e1a111"),
                        //Students = new List<User>
                        // {
                        //     userlist[2].User,
                        //     userlist[4].User
                        // }
                    },
                    new Lesson()
                    {
                        Id = Guid.Parse("b25a9294-35eb-447d-9717-6abcf3e4ad64"),
                        LessonDescription = "New topic 3",
                        StartDate = DateTime.UtcNow.AddDays(8),
                        DurationInMin = 80,
                        IsIndividual = true,
                        Status = LessonStatus.ClosedForRecord,
                        Price = 1000,
                        TeacherId = Guid.Parse("2c73101f-d2d4-46e4-9e29-93459dc71bc7"),
                        SubjectId = Guid.Parse("991533d5-d8d5-4a14-9c7b-eb9f14e1a111"),
                        // Students = new List<User>
                        // {
                        //     userlist[2].User,
                        // },
                    },
                    new Lesson()
                    {
                        Id = Guid.Parse("437ac77f-d0c1-4120-b391-18fb9ec5301e"),
                        LessonDescription = "New topic 4",
                        StartDate = DateTime.UtcNow.AddDays(2),
                        DurationInMin = 120,
                        IsIndividual = true,
                        Status = LessonStatus.OpenedForRecord,
                        Price = 1400,
                        TeacherId = Guid.Parse("0d3acdc7-ee94-4e22-8edc-aaaf92072eac"),
                        SubjectId = Guid.Parse("991533d5-d8d5-4a14-9c7b-eb9f14e1a222"),
                        //Students = new List<User>()
                    },
                    new Lesson()
                    {
                        Id = Guid.Parse("a8a17dac-85bf-44ae-be0e-a8f41c1d5ab6"),
                        LessonDescription = "New topic 5",
                        StartDate = DateTime.UtcNow.AddDays(4),
                        DurationInMin = 60,
                        IsIndividual = true,
                        Status = LessonStatus.OpenedForRecord,
                        Price = 1500,
                        TeacherId = Guid.Parse("0d3acdc7-ee94-4e22-8edc-aaaf92072eac"),
                        SubjectId = Guid.Parse("991533d5-d8d5-4a14-9c7b-eb9f14e1a222"),
                        //Students = new List<User>()
                    },
                    new Lesson()
                    {
                        Id = Guid.Parse("84ecf821-239a-4fdb-8005-6d305db4a3de"),
                        LessonDescription = "New topic 6",
                        StartDate = DateTime.UtcNow.AddDays(8),
                        DurationInMin = 45,
                        IsIndividual = true,
                        Status = LessonStatus.Canceled,
                        Price = 1500,
                        TeacherId = Guid.Parse("0d3acdc7-ee94-4e22-8edc-aaaf92072eac"),
                        SubjectId = Guid.Parse("991533d5-d8d5-4a14-9c7b-eb9f14e1a222"),
                        //Students = new List<User>()
                    }
                };
                return result;
            }
        }
    }
}
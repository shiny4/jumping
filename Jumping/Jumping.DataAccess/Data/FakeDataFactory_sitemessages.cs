using System;
using System.Collections.Generic;
using Jumping.Domain.Models;

namespace Jumping.DataAccess.Data
{
    public partial class FakeDataFactory
    {
        public static List<SiteMessage> SiteMessages => new List<SiteMessage>()
        {
            new SiteMessage()
            {
                Id = new Guid(),
                FromUserId = Guid.Parse("4122d412-682a-4bad-9837-ceda4824e261"),
                ToUserId = Guid.Parse("0d3acdc7-ee94-4e22-8edc-aaaf92072eac"),
                Theme = "Theme1",
                Content = "Help me!",
            },
            new SiteMessage()
            {
                Id = new Guid(),
                FromUserId = Guid.Parse("21092523-4d24-4909-b774-751de633ed95"),
                ToUserId = Guid.Parse("2c73101f-d2d4-46e4-9e29-93459dc71bc7"),
                Theme = "Theme2",
                Content = "Help me!",
            },
        };
    }
}
using System;
using System.Collections.Generic;
using Jumping.Domain.Enums;
using Jumping.Domain.Models;

namespace Jumping.DataAccess.Data
{
    public partial class FakeDataFactory
    {
        public static List<Ticket> Tickets => new List<Ticket>()
        {
            new Ticket()
            {
                Id = Guid.Parse("478b51de-1c5e-49c5-96df-eebf1e94329b"),
                FromUserId = Guid.Parse("0d3acdc7-ee94-4e22-8edc-aaaf92072eac"),
                Theme = "Theme1",
                Content = "Help me!",
                AdminAnswer = "ok!",
            },
            new Ticket()
            {
                Id = Guid.Parse("2241b9a7-c3d5-419d-a406-3323144a0e57"),
                FromUserId = Guid.Parse("2c73101f-d2d4-46e4-9e29-93459dc71bc7"),
                Theme = "Theme2",
                Content = "Help me!",
                AdminAnswer = "ok!",
                PreviosTicketId = Guid.Parse("478b51de-1c5e-49c5-96df-eebf1e94329b"),
            },
            new Ticket()
            {
                Id = Guid.Parse("d89dec61-2a32-4b75-90b2-a2cc8f22227e"),
                FromUserId = Guid.Parse("4122d412-682a-4bad-9837-ceda4824e261"),
                Theme = "Theme3",
                Content = "Help me!",
                AdminAnswer = "ok!",
                PreviosTicketId = Guid.Parse("2241b9a7-c3d5-419d-a406-3323144a0e57"),
            },
            new Ticket()
            {
                Id = Guid.Parse("125cb548-c4d0-4f22-82fa-5158a37672d4"),
                FromUserId = Guid.Parse("21092523-4d24-4909-b774-751de633ed95"),
                Theme = "Theme4",
                Content = "Help me!",
                AdminAnswer = "ok!",
            }
        };
    }
}
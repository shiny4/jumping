﻿using System;
using System.Collections.Generic;
using Jumping.Domain.Enums;
using Jumping.Domain.Models;

namespace Jumping.DataAccess.Data
{
    public partial class FakeDataFactory
    {
        public static IEnumerable<Subject> Subjects => new List<Subject>()
        {
            new Subject()
            {
                Id = Guid.Parse("991533d5-d8d5-4a14-9c7b-eb9f14e1a111"),
                Name = "Math",
                Category = Category.CategoryEnum.Estnauch
            },
            new Subject()
            {
                Id = Guid.Parse("991533d5-d8d5-4a14-9c7b-eb9f14e1a222"),
                Name = "C# language",
                Category = Category.CategoryEnum.It
            },
            new Subject()
            {
                Id = Guid.Parse("991533d5-d8d5-4a14-9c7b-eb9f14e1a333"),
                Name = "Engish",
                Category = Category.CategoryEnum.Guman
            }
        };
    }
}
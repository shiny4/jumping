﻿using System;
using System.Collections.Generic;
using Jumping.Domain.Models;

namespace Jumping.DataAccess.Data
{
    public partial class FakeDataFactory
    {
        public static IEnumerable<Application> Applications => new List<Application>()
        {
            new Application()
            {
                Id = Guid.NewGuid(),
                StudentId = Guid.Parse("4122d412-682a-4bad-9837-ceda4824e261"),
                SubjectId = Guid.Parse("991533d5-d8d5-4a14-9c7b-eb9f14e1a333"),
                TeacherId = Guid.Parse("2c73101f-d2d4-46e4-9e29-93459dc71bc7"),
                DesiredDateTime = DateTime.Now.ToString(),
                Description = "I want to study English",
                ActuallUntil = DateTime.Now.AddDays(3),
                CreationDate = DateTime.Now.AddDays(1)
            },
            new Application()
            {
                Id = Guid.NewGuid(),
                StudentId = Guid.Parse("21092523-4d24-4909-b774-751de633ed95"),
                SubjectId = Guid.Parse("991533d5-d8d5-4a14-9c7b-eb9f14e1a111"),
                TeacherId = Guid.Parse("0d3acdc7-ee94-4e22-8edc-aaaf92072eac"),
                DesiredDateTime = DateTime.Now.ToString(),
                Description = "I want to study math",
                ActuallUntil = DateTime.Now.AddDays(6),
                CreationDate = DateTime.Now.AddDays(3)
            }
        };
    }
}
using System;
using System.Collections.Generic;
using Jumping.Domain.Enums;
using Jumping.Domain.Models;

namespace Jumping.DataAccess.Data
{
    public partial class FakeDataFactory
    {
        public static List<CanceledLesson> CanceledLessons => new List<CanceledLesson>()
        {
            new CanceledLesson()
            {
                Id = Guid.Parse("dfeba61e-8c05-4665-bdb2-5a6c4418b528"),
                InitiatorId = Guid.Parse("2c73101f-d2d4-46e4-9e29-93459dc71bc7"),
                LessonId = Guid.Parse("84ecf821-239a-4fdb-8005-6d305db4a3de"),
                Reason = "sfsdfsd sdfsdfsdf",
                Role = RoleEnum.Teacher,
                CancelDate = DateTime.Now
            }
        };
    }
}
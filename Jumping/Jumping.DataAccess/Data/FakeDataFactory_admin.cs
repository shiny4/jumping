using Jumping.Domain.Enums;
using Jumping.Domain.Models;
using Jumping.Domain.Models.Administration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jumping.DataAccess.Data
{
    public partial class FakeDataFactory
    {
        public static IEnumerable<Admin> Admins => new List<Admin>()
        {
            new Admin()
            {
                Id = new Guid(),
                FirstName = "User1",
                LastName = "LastName",
                Password = "123",
                PasswordSalt = "salt",
                Email = "sdfsfd",
                PhoneNumber = "234",
            },
        };
    }
}
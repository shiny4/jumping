using Jumping.Domain.Enums;
using Jumping.Domain.Models;
using Jumping.Domain.Models.Administration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jumping.DataAccess.Data
{
    public partial class FakeDataFactory
    {
        public static List<UserAuth> AuthUsers => new List<UserAuth>()
        {
            new UserAuth()
            {
                Id = Guid.Parse("2c73101f-d2d4-46e4-9e29-93459dc71bc7"),
                Login = "login1",
                Password = "111",
                PasswordSalt = "newSalt1",
                Status = UserStatusEnum.Active,
                User = new User()
                {
                    Id = Guid.Parse("2c73101f-d2d4-46e4-9e29-93459dc71bc7"),
                    UserAuthId = Guid.Parse("2c73101f-d2d4-46e4-9e29-93459dc71bc7"),
                    NotificationPreferenceId = Guid.Parse("2c73101f-d2d4-46e4-9e29-93459dc71bc7"),
                    FirstName = "Teacher1",
                    LastName = "Teacheror1",
                    Email = "sdfsfdsf@gdrgr.tt",
                    PhoneNumber = "111-111-1111",
                    RatingAsTeacher = 10,
                    RatingAsUser = -2,
                    Portfolio =
                        "EF Core 5 автоматически создает промежуточную таблицу с двумя столбцами, через которые она связана с двумя другими таблицами. Однако иногда может потребоваться добавить в промежуточную таблицу еще какие-то данные. Например, в случае со студентами и курсами мы бы могли хранить в промежуточной таблице также дату поступления студента на выбранный курс. В этом случае на уровне кода C# лучше создать помежуточную сущность, которая будет содержать описание данных, которые мы хотим определить",
                    DesiredTimeForLessons = "каждый день после 17.00"
                },
                NotificationPreference = new NotificationPreference
                {
                    NoteAboutAnswer = true,
                    NoteAboutLesson = NotificationEnum.Befor30Min,
                    NoteAboutMessage = false,
                    UserAuthId = Guid.Parse("2c73101f-d2d4-46e4-9e29-93459dc71bc7"),
                    Id = Guid.Parse("2c73101f-d2d4-46e4-9e29-93459dc71bc7"),
                }
            },
            new UserAuth()
            {
                Id = Guid.Parse("0d3acdc7-ee94-4e22-8edc-aaaf92072eac"),
                Login = "login2",
                Password = "222",
                PasswordSalt = "newSalt2",
                Status = UserStatusEnum.Active,
                User = new User()
                {
                    Id = Guid.Parse("0d3acdc7-ee94-4e22-8edc-aaaf92072eac"),
                    UserAuthId = Guid.Parse("0d3acdc7-ee94-4e22-8edc-aaaf92072eac"),
                    NotificationPreferenceId = Guid.Parse("0d3acdc7-ee94-4e22-8edc-aaaf92072eac"),
                    FirstName = "Teacher2",
                    LastName = "Teacheror2",
                    Email = "hfhgfhfgh@sgsdgd.tt",
                    PhoneNumber = "222-222-2222",
                    RatingAsTeacher = 170,
                    RatingAsUser = 43,
                    Portfolio =
                        "Платформу Entity Framework Core можно применять в различных технологиях стека .NET. В данном случае мы будем рассматривать базовые моменты платформы на примере консольных приложений, как наиболее простых и не содержащих никакого лишнего кода. Но в последствии также затронем применение EF Core и в других технологиях на конкретных примерах.",
                    DesiredTimeForLessons = "каждый день после 20.00",
                },
                NotificationPreference = new NotificationPreference
                {
                    NoteAboutAnswer = false,
                    NoteAboutLesson = NotificationEnum.BeforeOneHour,
                    NoteAboutMessage = true,
                    UserAuthId = Guid.Parse("0d3acdc7-ee94-4e22-8edc-aaaf92072eac"),
                    Id = Guid.Parse("0d3acdc7-ee94-4e22-8edc-aaaf92072eac"),
                }
            },
            new UserAuth()
            {
                Id = Guid.Parse("4122d412-682a-4bad-9837-ceda4824e261"),
                Login = "login3",
                Password = "333",
                PasswordSalt = "newSalt3",
                Status = UserStatusEnum.Active,
                User = new User()
                {
                    Id = Guid.Parse("4122d412-682a-4bad-9837-ceda4824e261"),
                    UserAuthId = Guid.Parse("4122d412-682a-4bad-9837-ceda4824e261"),
                    NotificationPreferenceId = Guid.Parse("4122d412-682a-4bad-9837-ceda4824e261"),
                    FirstName = "Student1",
                    LastName = "dfdsgds",
                    Email = "gfhfghf@dhdh.uu",
                    PhoneNumber = "333-333-3333",
                    RatingAsTeacher = 0,
                    RatingAsUser = 67,
                    Portfolio =
                        "Git – система контроля версий. GitHub –  сайт, предоставляющий Git-хостинг.  Он служит заменой Git-сервера, если не хочется разворачивать сервер самостоятельно.  Это место, где можно создать удаленный репозиторий и хранить там проект.",
                    DesiredTimeForLessons = "каждый день c 14.00 до 18.00",
                },
                NotificationPreference = new NotificationPreference
                {
                    NoteAboutAnswer = true,
                    NoteAboutLesson = NotificationEnum.Before10Min,
                    NoteAboutMessage = false,
                    UserAuthId = Guid.Parse("4122d412-682a-4bad-9837-ceda4824e261"),
                    Id = Guid.Parse("4122d412-682a-4bad-9837-ceda4824e261"),
                }
            },
            new UserAuth()
            {
                Id = Guid.Parse("21092523-4d24-4909-b774-751de633ed95"),
                Login = "login4",
                Password = "444",
                PasswordSalt = "newSalt4",
                Status = UserStatusEnum.Active,
                User = new User()
                {
                    Id = Guid.Parse("21092523-4d24-4909-b774-751de633ed95"),
                    UserAuthId = Guid.Parse("21092523-4d24-4909-b774-751de633ed95"),
                    NotificationPreferenceId = Guid.Parse("21092523-4d24-4909-b774-751de633ed95"),
                    FirstName = "Student2",
                    LastName = "gjgjga",
                    Email = "hjkhjk@wrwr.rr",
                    PhoneNumber = "444-444-4444",
                    RatingAsTeacher = 5,
                    RatingAsUser = 200,
                    Portfolio =
                        "После установки Git вы сможете его запустить в нужной папке, щелкнув ее правой кнопкой мыши и выбрав пункт “Git Bash Here” – очень удобная опция, так как не придется переходить в репозиторий из командной строки. То есть щелкать правой кнопкой мыши надо будет папку-репозиторий.",
                    DesiredTimeForLessons = "в выходные дни",
                },
                NotificationPreference = new NotificationPreference
                {
                    Id = Guid.Parse("21092523-4d24-4909-b774-751de633ed95"),
                    UserAuthId = Guid.Parse("21092523-4d24-4909-b774-751de633ed95"),
                    NoteAboutAnswer = true,
                    NoteAboutLesson = NotificationEnum.BeforeOneDay,
                    NoteAboutMessage = true,
                }
            },
            new UserAuth()
            {
                Id = Guid.Parse("c2589eaf-3bc2-43d1-a101-bad3248fa4e3"),
                Login = "login5",
                Password = "555",
                PasswordSalt = "newSalt5",
                Status = UserStatusEnum.Active,
                User = new User()
                {
                    Id = Guid.Parse("c2589eaf-3bc2-43d1-a101-bad3248fa4e3"),
                    UserAuthId = Guid.Parse("c2589eaf-3bc2-43d1-a101-bad3248fa4e3"),
                    NotificationPreferenceId = Guid.Parse("c2589eaf-3bc2-43d1-a101-bad3248fa4e3"),
                    FirstName = "Student3",
                    LastName = "fdhfh",
                    Email = "lll@oo.rr",
                    PhoneNumber = "555-555-5555",
                    RatingAsTeacher = 0,
                    RatingAsUser = 123,
                    Portfolio =
                        "В какой-то момент история репозитория ветвится. Например, в проекте требуется пофиксить баг –  обычно это выполняют в отдельной ветке, а потом сливают в основную ветку master.  Команда git merge как раз и служит для слияния веток.",
                    DesiredTimeForLessons = "каждый день после 21.00",
                },
                NotificationPreference = new NotificationPreference
                {
                    Id = Guid.Parse("c2589eaf-3bc2-43d1-a101-bad3248fa4e3"),
                    UserAuthId = Guid.Parse("c2589eaf-3bc2-43d1-a101-bad3248fa4e3"),
                    NoteAboutAnswer = false,
                    NoteAboutLesson = NotificationEnum.BeforeOneHour,
                    NoteAboutMessage = false,
                }
            }
        };


        // public static List<User> Users => new List<User>()
        // {
        //     new User()
        //     {
        //         Id = Guid.Parse("2c73101f-d2d4-46e4-9e29-93459dc71bc7"),
        //         UserAuth = new UserAuth()
        //         {
        //             Id = Guid.Parse("2c73101f-d2d4-46e4-9e29-93459dc71bc7"),
        //             Login = "login1",
        //             Password = "111",
        //             PasswordSalt = "newSalt1",
        //             Status = UserStatusEnum.Active
        //         },
        //         NotificationPreference = new NotificationPreference
        //         {
        //             NoteAboutAnswer = true,
        //             NoteAboutLesson = NotificationEnum.Befor30Min,
        //             NoteAboutMessage = false,
        //         },
        //
        //         FirstName = "Teacher1",
        //         LastName = "Teacheror1",
        //         Email = "sdfsfdsf@gdrgr.tt",
        //         PhoneNumber = "111-111-1111",
        //         RatingAsTeacher = 10,
        //         RatingAsUser = -2,
        //         Portfolio =
        //             "EF Core 5 автоматически создает промежуточную таблицу с двумя столбцами, через которые она связана с двумя другими таблицами. Однако иногда может потребоваться добавить в промежуточную таблицу еще какие-то данные. Например, в случае со студентами и курсами мы бы могли хранить в промежуточной таблице также дату поступления студента на выбранный курс. В этом случае на уровне кода C# лучше создать помежуточную сущность, которая будет содержать описание данных, которые мы хотим определить",
        //         DesiredTimeForLessons = "каждый день после 17.00",
        //     },
        //     new User()
        //     {
        //         Id = Guid.Parse("0d3acdc7-ee94-4e22-8edc-aaaf92072eac"),
        //         UserAuth = new UserAuth()
        //         {
        //             Id = Guid.Parse("0d3acdc7-ee94-4e22-8edc-aaaf92072eac"),
        //             Login = "login2",
        //             Password = "222",
        //             PasswordSalt = "newSalt2",
        //             Status = UserStatusEnum.Active,
        //         },
        //         NotificationPreference = new NotificationPreference
        //         {
        //             NoteAboutAnswer = false,
        //             NoteAboutLesson = NotificationEnum.BeforeOneHour,
        //             NoteAboutMessage = true,
        //         },
        //         FirstName = "Teacher2",
        //         LastName = "Teacheror2",
        //         Email = "hfhgfhfgh@sgsdgd.tt",
        //         PhoneNumber = "222-222-2222",
        //         RatingAsTeacher = 170,
        //         RatingAsUser = 43,
        //         Portfolio =
        //             "Платформу Entity Framework Core можно применять в различных технологиях стека .NET. В данном случае мы будем рассматривать базовые моменты платформы на примере консольных приложений, как наиболее простых и не содержащих никакого лишнего кода. Но в последствии также затронем применение EF Core и в других технологиях на конкретных примерах.",
        //         DesiredTimeForLessons = "каждый день после 20.00",
        //     },
        //
        //     new User()
        //     {
        //         Id = Guid.Parse("4122d412-682a-4bad-9837-ceda4824e261"),
        //         UserAuth = new UserAuth()
        //         {
        //             Id = Guid.Parse("4122d412-682a-4bad-9837-ceda4824e261"),
        //             Login = "login3",
        //             Password = "333",
        //             PasswordSalt = "newSalt3",
        //             Status = UserStatusEnum.Active,
        //         },
        //         NotificationPreference = new NotificationPreference
        //         {
        //             NoteAboutAnswer = true,
        //             NoteAboutLesson = NotificationEnum.Before10Min,
        //             NoteAboutMessage = false,
        //         },
        //         FirstName = "Student1",
        //         LastName = "dfdsgds",
        //         Email = "gfhfghf@dhdh.uu",
        //         PhoneNumber = "333-333-3333",
        //         RatingAsTeacher = 0,
        //         RatingAsUser = 67,
        //         Portfolio =
        //             "Git – система контроля версий. GitHub –  сайт, предоставляющий Git-хостинг.  Он служит заменой Git-сервера, если не хочется разворачивать сервер самостоятельно.  Это место, где можно создать удаленный репозиторий и хранить там проект.",
        //         DesiredTimeForLessons = "каждый день c 14.00 до 18.00",
        //     },
        //
        //     new User()
        //     {
        //         Id = Guid.Parse("21092523-4d24-4909-b774-751de633ed95"),
        //         UserAuth = new UserAuth()
        //         {
        //             Id = Guid.Parse("21092523-4d24-4909-b774-751de633ed95"),
        //             Login = "login4",
        //             Password = "444",
        //             PasswordSalt = "newSalt4",
        //             Status = UserStatusEnum.Active,
        //         },
        //         NotificationPreference = new NotificationPreference
        //         {
        //             NoteAboutAnswer = true,
        //             NoteAboutLesson = NotificationEnum.BeforeOneDay,
        //             NoteAboutMessage = true,
        //         },
        //         FirstName = "Student2",
        //         LastName = "gjgjga",
        //         Email = "hjkhjk@wrwr.rr",
        //         PhoneNumber = "444-444-4444",
        //         RatingAsTeacher = 5,
        //         RatingAsUser = 200,
        //         Portfolio =
        //             "После установки Git вы сможете его запустить в нужной папке, щелкнув ее правой кнопкой мыши и выбрав пункт “Git Bash Here” – очень удобная опция, так как не придется переходить в репозиторий из командной строки. То есть щелкать правой кнопкой мыши надо будет папку-репозиторий.",
        //         DesiredTimeForLessons = "в выходные дни",
        //     },
        //
        //     new User()
        //     {
        //         Id = Guid.Parse("c2589eaf-3bc2-43d1-a101-bad3248fa4e3"),
        //         UserAuth = new UserAuth()
        //         {
        //             Id = Guid.Parse("c2589eaf-3bc2-43d1-a101-bad3248fa4e3"),
        //             Login = "login5",
        //             Password = "555",
        //             PasswordSalt = "newSalt5",
        //             Status = UserStatusEnum.Active,
        //         },
        //         NotificationPreference = new NotificationPreference
        //         {
        //             NoteAboutAnswer = false,
        //             NoteAboutLesson = NotificationEnum.BeforeOneHour,
        //             NoteAboutMessage = false,
        //         },
        //         FirstName = "Student3",
        //         LastName = "fdhfh",
        //         Email = "lll@oo.rr",
        //         PhoneNumber = "555-555-5555",
        //         RatingAsTeacher = 0,
        //         RatingAsUser = 123,
        //         Portfolio =
        //             "В какой-то момент история репозитория ветвится. Например, в проекте требуется пофиксить баг –  обычно это выполняют в отдельной ветке, а потом сливают в основную ветку master.  Команда git merge как раз и служит для слияния веток.",
        //         DesiredTimeForLessons = "каждый день после 21.00",
        //     }
        // };
    }
}
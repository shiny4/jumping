﻿using System;
using System.Collections.Generic;
using Jumping.Domain.Models;

namespace Jumping.DataAccess.Data
{
    public partial class FakeDataFactory
    {
        public static IEnumerable<Review> Reviews => new List<Review>()
        {
            new Review()
            {
                Id = Guid.Parse("2ad17d8b-83a0-438a-bae3-da85eb2f7362"),
                Description = "Great job",
                Score = 20,
                LessonId = Guid.Parse("5a3a7b57-3f09-4763-a375-b9f7c1699fd9"),
                OnWhomId = Guid.Parse("2c73101f-d2d4-46e4-9e29-93459dc71bc7"),
                AutherId = Guid.Parse("4122d412-682a-4bad-9837-ceda4824e261")
            },
            new Review()
            {
                Id = Guid.Parse("3e9ceaa8-8bd4-4667-8d11-e80a9ef9d13a"),
                Description = "Well done",
                Score = 40,
                LessonId = Guid.Parse("5b94ba37-52c3-4ffd-9968-795c60e9b0ba"),
                OnWhomId = Guid.Parse("0d3acdc7-ee94-4e22-8edc-aaaf92072eac"),
                AutherId = Guid.Parse("21092523-4d24-4909-b774-751de633ed95")
            }
        };
    }
}

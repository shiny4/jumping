﻿using Jumping.DataAccess.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jumping.DataAccess
{
    public class DatabaseInitializer : IDatabaseInitializer
    {
        private readonly DataContext _dataContext;

        public DatabaseInitializer(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public DatabaseInitializer()
        {
            _dataContext = null;
        }

        public void Initialize()
        {
            if (true)
            {
                _dataContext.Database.EnsureDeleted();
                _dataContext.Database.EnsureCreated();
                AddContent(_dataContext);
            }
        }

        public void Initialize(DataContext dbContext)
        {
            if (false)
            {
                dbContext.Database.EnsureDeleted();
            }

            dbContext.Database.EnsureCreated();
            AddContent(dbContext);
        }

        public void AddContent(DataContext dbContext)
        {
            dbContext.Admins.AddRange(FakeDataFactory.Admins);
            dbContext.Subjects.AddRange(FakeDataFactory.Subjects);

            dbContext.UserAuths.AddRange(FakeDataFactory.AuthUsers);
            dbContext.Lessons.AddRange(FakeDataFactory.Lessons);
            dbContext.Tickets.AddRange(FakeDataFactory.Tickets);
            dbContext.SiteMessages.AddRange(FakeDataFactory.SiteMessages);

            dbContext.Applications.AddRange(FakeDataFactory.Applications);
            dbContext.Reviews.AddRange(FakeDataFactory.Reviews);
            dbContext.CanceledLessons.AddRange(FakeDataFactory.CanceledLessons);

            //
            // //dbContext.Database.Migrate();
            dbContext.SaveChanges();
        }
    }
}
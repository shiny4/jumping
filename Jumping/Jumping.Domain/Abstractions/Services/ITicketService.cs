﻿using Jumping.Domain.Common.DTO;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Jumping.Domain.Abstractions.Services
{
    public interface ITicketService
    {
        Task<ICollection<TicketResponseDTO>> GetAllTicketsAsync();

        Task<TicketResponseDTO> GetTicketByIdAsync(Guid id);

        Task<string> AddNewTicketAsync(CreateOrUpdateTicketDTO ticket);

        Task<string> UpdateTicketAsync(Guid id, CreateOrUpdateTicketDTO ticket);

        Task<string> RemoveTicketAsync(Guid id);
    }
}
﻿using Jumping.Domain.Common.DTO;
using Jumping.Domain.Models.Administration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jumping.Domain.Abstractions.Services
{
    public interface IAdminService
    {
        Task<ICollection<AdminResponseDTO>> GetAllAdminsAsync();

        // Task<ICollection<AdminResponseDTO>> GetAllAdminsAsync();

        Task<string> AddNewAdminAsync(CreateOrUpdateAdminDTO adminDTO);

        Task RemoveAdminAsync(Guid Id);

        Task UpdateAdminAsync(Guid id, CreateOrUpdateAdminDTO adminDTO);

        Task<AdminResponseDTO> GetAdminByIDAsync(Guid id);
    }
}
﻿using Jumping.Domain.Common.DTO;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Jumping.Domain.Abstractions.Services
{
    public interface ICanceledLessonService
    {
        Task<ICollection<CanceledLessonResponseDTO>> GetAllCanceledLessonsAsync();

        Task<CanceledLessonResponseDTO> GetCanceledLessonByIdAsync(Guid id);

        Task<string> AddNewCanceledLessonAsync(CreateOrUpdateCanceledLessonDTO canceledLesson);

        Task<string> UpdateCanceledLessonAsync(Guid id, CreateOrUpdateCanceledLessonDTO canceledLesson);

        Task<string> RemoveCanceledLessonAsync(Guid id);
    }
}
﻿using Jumping.Domain.Common.DTO;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Jumping.Domain.Abstractions.Services
{
    public interface ILessonService
    {
        Task<ICollection<LessonResponseDTO>> GetAllLessonsAsync();

        Task<LessonResponseDTO> GetLessonByIdAsync(Guid id);

        Task<string> AddNewLessonAsync(CreateOrUpdateLessonDTO lesson);

        Task<string> UpdateLessonAsync(Guid id, CreateOrUpdateLessonDTO lesson);

        Task<string> RemoveLessonAsync(Guid id);
    }
}
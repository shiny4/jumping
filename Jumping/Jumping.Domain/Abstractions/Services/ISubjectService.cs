﻿using Jumping.Domain.Common.DTO;
using Jumping.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jumping.Domain.Abstractions.Services
{
    public interface ISubjectService
    {
        Task<ICollection<SubjectResponseDTO>> GetAllSubjects();
        Task<string> AddSubjectAsync(CreateOrUpdateSubjectDTO request);
        Task RemoveSubjectAsync(Guid id);
        Task UpdateSubjectAsync(Guid id, CreateOrUpdateSubjectDTO request);
        Task<SubjectResponseDTO> GetSubjectByID(Guid id);
    }
}

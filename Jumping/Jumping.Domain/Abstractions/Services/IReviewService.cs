﻿using Jumping.Domain.Common.DTO;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Jumping.Domain.Abstractions.Services
{
    public interface IReviewService
    {
        Task<ICollection<ReviewResponseDTO>> GetAllReviewsAsync();

        Task<ReviewResponseDTO> GetReviewByIdAsync(Guid id);

        Task<string> AddNewReviewAsync(CreateOrUpdateReviewDTO review);

        Task<string> UpdateReviewAsync(Guid id, CreateOrUpdateReviewDTO review);

        Task<string> RemoveReviewAsync(Guid id);

    }
}

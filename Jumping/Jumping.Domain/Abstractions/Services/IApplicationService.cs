﻿using Jumping.Domain.Common.DTO;
using Jumping.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jumping.Domain.Abstractions.Services
{
    public interface IApplicationService
    {
        Task<ICollection<ApplicationResponseDTO>> GetAllApplications();
        Task<string> AddApplicationAsync(CreateOrUpdateApplicationDTO request);
        Task RemoveApplicationAsync(Guid id);
        Task UpdateApplicationAsync(Guid id, CreateOrUpdateApplicationDTO request);
        Task<ApplicationResponseDTO> GetApplicationByID(Guid id);
    }
}

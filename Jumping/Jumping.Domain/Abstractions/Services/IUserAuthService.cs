﻿using Jumping.Domain.Common.DTO;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Jumping.Domain.Abstractions.Services
{
    public interface IUserService
    {
        Task<ICollection<UserResponseDTO>> GetAllUsersAsync();

        Task<UserResponseDTO> GetUserByIdAsync(Guid id);

        Task<string> AddNewUserAsync(CreateOrUpdateUserDTO user);

        Task<string> UpdateUserAsync(Guid id, CreateOrUpdateUserDTO user);

        //Task<string> RemoveUserAsync(Guid id);
    }
}
﻿using Jumping.Domain.Common.DTO;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Jumping.Domain.Abstractions.Services
{
    public interface ISiteMessageService
    {
        Task<ICollection<SiteMessageResponseDTO>> GetAllSiteMessagesAsync();

        Task<SiteMessageResponseDTO> GetSiteMessageByIdAsync(Guid id);

        Task<string> AddNewSiteMessageAsync(CreateOrUpdateSiteMessageDTO siteMessage);

        Task<string> UpdateSiteMessageAsync(Guid id, CreateOrUpdateSiteMessageDTO siteMessage);

        Task<string> RemoveSiteMessageAsync(Guid id);
    }
}
﻿using Jumping.Domain.Common.DTO;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Jumping.Domain.Abstractions.Services
{
    public interface IUserAuthService
    {
        Task<ICollection<UserAuthResponseDTO>> GetAllUserAuthsAsync();

        Task<UserAuthResponseDTO> GetUserAuthByIdAsync(Guid id);

        Task<string> AddNewUserAuthAsync(CreateOrUpdateUserAuthDTO user);

        Task<string> UpdateUserAuthAsync(Guid id, CreateOrUpdateUserAuthDTO user);

        Task<string> RemoveUserAuthAsync(Guid id);
    }
}
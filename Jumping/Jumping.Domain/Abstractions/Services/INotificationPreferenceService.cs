﻿using Jumping.Domain.Common.DTO;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Jumping.Domain.Abstractions.Services
{
    public interface INotificationPreferenceService
    {
        Task<ICollection<NotificationPreferenceResponseDTO>> GetAllNotificationPreferencesAsync();

        Task<NotificationPreferenceResponseDTO> GetNotificationPreferenceByIdAsync(Guid id);

        Task<string> AddNewNotificationPreferenceAsync(CreateOrUpdateNotificationPreferenceDTO notificationPreference);

        Task<string> UpdateNotificationPreferenceAsync(Guid id,
            CreateOrUpdateNotificationPreferenceDTO notificationPreference);

        //Task<string> RemoveNotificationPreferenceAsync(Guid id);
    }
}
using Jumping.Domain.Models;

namespace Jumping.Domain.Abstractions.Repositories
{
    public interface ITicketRepository : IBaseRepository<Ticket>
    {
    }
}
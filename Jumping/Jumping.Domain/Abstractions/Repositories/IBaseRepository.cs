﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Jumping.Domain.Abstractions.Repositories
{
    public interface IBaseRepository<TEntity>
        where TEntity : class
    {
        Task<ICollection<TEntity>> GetAllAsync();

        Task<ICollection<TEntity>> GetAsync(Expression<Func<TEntity, bool>> predicate, int maxItemsAmount = 0);

        Task<TEntity> GetByIdAsync(Guid id);

        Task<Guid> CreateAsync(TEntity entity);

        Task CreateBulkAsync(ICollection<TEntity> entities);

        Task UpdateAsync(TEntity item);

        Task DeleteAsync(TEntity item);

        Task DeleteBulkAsync(ICollection<TEntity> items);
    }
}

﻿using Jumping.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jumping.Domain.Abstractions.Repositories
{
    public interface IApplicationRepository : IBaseRepository<Application>
    {
        Task<ICollection<Application>> GetAllApplicationsWithSubjectsTeachersStudentsAsync();

        Task<Application> GetApplicationForUpdateAsync(Guid id);

        Task<Application> GetApplicationWithSubjectTeacherStudentByIdAsync(Guid id);
    }
}

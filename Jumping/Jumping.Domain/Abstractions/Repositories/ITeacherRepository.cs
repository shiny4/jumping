﻿using Jumping.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jumping.Domain.Abstractions.Repositories
{
    public interface ITeacherRepository : IBaseRepository<User>
    {
        Task<ICollection<User>> GetTeachersWithRolesAsync();

        Task<User> GetTeacherForUpdateAsync(Guid id);

        Task<User> GetTeacherWithRoleAsync(Guid id);
    }
}
﻿using Jumping.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jumping.Domain.Abstractions.Repositories
{
    public interface IReviewRepository : IBaseRepository<Review>
    {
        Task<ICollection<Review>> GetAllReviewsWithTeachersStudentsAsync();

        Task<Review> GetReviewForUpdateAsync(Guid id);

        Task<Review> GetReviewWithTeacherStudentByIdAsync(Guid id);
    }
}

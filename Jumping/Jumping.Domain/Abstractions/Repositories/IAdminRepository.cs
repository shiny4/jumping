﻿using Jumping.Domain.Models.Administration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jumping.Domain.Abstractions.Repositories
{
    public interface IAdminRepository : IBaseRepository<Admin>
    {
        Task<ICollection<Admin>> GetAdminsAsync();

        Task<Admin> GetAdminForUpdateAsync(Guid id);

        Task<Admin> GetAdminAsync(Guid id);
    }
}
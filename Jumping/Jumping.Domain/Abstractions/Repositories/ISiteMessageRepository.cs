using Jumping.Domain.Models;

namespace Jumping.Domain.Abstractions.Repositories
{
    public interface ISiteMessageRepository : IBaseRepository<SiteMessage>
    {
    }
}
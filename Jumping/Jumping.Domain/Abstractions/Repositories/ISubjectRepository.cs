﻿using Jumping.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jumping.Domain.Abstractions.Repositories
{
    public interface ISubjectRepository : IBaseRepository<Subject>
    {
        Task<ICollection<Subject>> GetAllSubjectsWithCategoriesAsync();

        Task<Subject> GetSubjectForUpdateAsync(Guid id);

        Task<Subject> GetSubjectWithCategoryByIdAsync(Guid id);
    }
}

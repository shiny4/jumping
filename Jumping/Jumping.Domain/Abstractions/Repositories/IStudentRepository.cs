﻿using Jumping.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jumping.Domain.Abstractions.Repositories
{
    public interface IStudentRepository : IBaseRepository<User>
    {
        Task<ICollection<User>> GetStudentsWithRolesAsync();

        Task<User> GetStudentWithRoleAsync(Guid id);

        Task<User> GetStudentForUpdateAsync(Guid id);
    }
}
namespace Jumping.Domain.Enums
{
    public enum LessonStatus
    {
        OpenedForRecord,
        ClosedForRecord,
        Over,
        Canceled
    }
}
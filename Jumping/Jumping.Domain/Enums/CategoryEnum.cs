namespace Jumping.Domain.Enums
{
    public static class Category
    {
        public enum CategoryEnum
        {
            None = 0,
            Guman,
            Estnauch,
            Techn,
            It,
            Primaryschool
        }

        public static string Value(CategoryEnum enumValue)
        {
            switch (enumValue)
            {
                case CategoryEnum.None:
                    return ("Без категории");
                case CategoryEnum.Estnauch:
                    return ("Естественные науки");
                case CategoryEnum.Guman:
                    return ("Гумманитарные науки");
                case CategoryEnum.It:
                    return ("Информационные технологии");
                case CategoryEnum.Primaryschool:
                    return ("Начальная школа");
                case CategoryEnum.Techn:
                    return ("Технические науки");
            }

            return ("");
        }
    }
}
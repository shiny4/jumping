namespace Jumping.Domain.Enums
{
    public enum UserStatusEnum
    {
        Active = 0,
        Blocked
    }
}
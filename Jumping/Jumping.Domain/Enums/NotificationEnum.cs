namespace Jumping.Domain.Enums
{
    public enum NotificationEnum
    {
        BeforeOneDay = 1440,
        BeforeSixHours = 360,
        BeforeTwoHours = 120,
        BeforeOneHour = 60,
        Befor30Min = 30,
        Before10Min = 10
    }
}
using System;
using System.Collections.Generic;

namespace Jumping.Domain.Models
{
    public class SiteMessage : BaseEntity
    {
        public Guid FromUserId { get; set; }
        public User FromUser { get; set; }

        public Guid ToUserId { get; set; }
        public User ToUser { get; set; }

        public Guid? PreviosMessageId { get; set; }
        public SiteMessage PreviosMessage { get; set; }

        public virtual ICollection<SiteMessage> MessageTree { get; set; }
        public string Theme { get; set; }

        public string Content { get; set; }
    }
}
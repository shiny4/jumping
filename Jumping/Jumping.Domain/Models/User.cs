﻿using Jumping.Domain.Models.Administration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Jumping.Domain.Enums;

namespace Jumping.Domain.Models
{
    public class User : BaseEntity
    {
        public User()
        {
            FirstName = "";
            LastName = "";
            Email = "";
            PhoneNumber = "";
            RatingAsUser = 0;
            RatingAsTeacher = 0;
            Photo = "";
            Portfolio = "";
            DesiredTimeForLessons = "";
        }

        public Guid UserAuthId { get; set; }
        public UserAuth UserAuth { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }

        public int RatingAsUser { get; set; }

        public int RatingAsTeacher { get; set; }

        public string Photo { get; set; }

        public string Portfolio { get; set; }

        public string DesiredTimeForLessons { get; set; }

        public Guid NotificationPreferenceId { get; set; }
        public NotificationPreference NotificationPreference { get; set; }

        public virtual ICollection<Application> InApplications { get; set; }

        public virtual ICollection<Application> OutApplications { get; set; }

        public virtual ICollection<SiteMessage> InSiteMessages { get; set; }

        public virtual ICollection<SiteMessage> OutSiteMessages { get; set; }

        public virtual ICollection<Subject> Subjects { get; set; }

        public virtual ICollection<CanceledLesson> CanceledLessons { get; set; }
        public virtual ICollection<Lesson> LessonsAsTeacher { get; set; }

        public virtual ICollection<Lesson> LessonsAsStudent { get; set; }

        public virtual ICollection<Review> ReviewsByMe { get; set; }

        public virtual ICollection<Review> ReviewsOnMe { get; set; }

        public virtual ICollection<Ticket> Tickets { get; set; }
    }
}
﻿using Jumping.Domain.Enums;
using Jumping.Domain.Models.Administration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jumping.Domain.Models
{
    public class Application : BaseEntity
    {
        public string Description { get; set; }
        public Guid? SubjectId { get; set; }
        public Subject Subject { get; set; }
        public Guid StudentId { get; set; }
        public User Student { get; set; }
        public Guid TeacherId { get; set; }
        public User Teacher { get; set; }

        public DateTime CreationDate { get; set; }

        public DateTime ActuallUntil { get; set; }

        public string DesiredDateTime { get; set; }
    }
}
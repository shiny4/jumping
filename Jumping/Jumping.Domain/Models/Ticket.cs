using System;
using System.Collections.Generic;

namespace Jumping.Domain.Models
{
    public class Ticket : BaseEntity
    {
        public Guid FromUserId { get; set; }
        public User FromUser { get; set; }

        public Guid? PreviosTicketId { get; set; }
        public Ticket PreviosTicket { get; set; }

        public virtual ICollection<Ticket> TicketTree { get; set; }

        public string Theme { get; set; }

        public string Content { get; set; }

        public string AdminAnswer { get; set; }
    }
}
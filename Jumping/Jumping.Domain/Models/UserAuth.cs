using System;
using Jumping.Domain.Enums;

namespace Jumping.Domain.Models
{
    public class UserAuth : BaseEntity
    {
        public string Login { get; set; }

        public string Password { get; set; }

        public string PasswordSalt { get; set; }

        public UserStatusEnum Status { get; set; }

        public User User { get; set; }

        //public Guid UserId { get; set; }
        public NotificationPreference NotificationPreference { get; set; }
    }
}
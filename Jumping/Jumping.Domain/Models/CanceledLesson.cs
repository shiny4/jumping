using System;
using Jumping.Domain.Enums;
using Jumping.Domain.Models.Administration;

namespace Jumping.Domain.Models
{
    public class CanceledLesson : BaseEntity
    {
        public Guid LessonId { get; set; }
        public Lesson Lesson { get; set; }

        public Guid InitiatorId { get; set; }
        public User Initiator { get; set; }

        public DateTime CancelDate { get; set; }

        public RoleEnum Role { get; set; }

        public string Reason { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jumping.Domain.Models
{
    public class Review : BaseEntity
    {
        public string Description { get; set; }

        public int Score { get; set; }
        public Guid? AutherId { get; set; }
        public User Auther { get; set; }
        public Guid OnWhomId { get; set; }
        public User OnWhom { get; set; }

        public Guid LessonId { get; set; }
        public Lesson Lesson { get; set; }
    }
}
using System;
using Jumping.Domain.Enums;

namespace Jumping.Domain.Models
{
    public class NotificationPreference : BaseEntity
    {
        public NotificationPreference()
        {
            NoteAboutMessage = true;
            NoteAboutAnswer = true;
            NoteAboutLesson = NotificationEnum.Befor30Min;
        }

        public Guid UserAuthId { get; set; }

        public UserAuth UserAuth { get; set; }

        // public Guid UserId { get; set; }
        // public User User { get; set; }
        public NotificationEnum NoteAboutLesson { get; set; }

        public bool NoteAboutMessage { get; set; }

        public bool NoteAboutAnswer { get; set; }
    }
}
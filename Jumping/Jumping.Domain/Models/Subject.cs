﻿using Jumping.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jumping.Domain.Models
{
    public class Subject : BaseEntity
    {
        public string Name { get; set; }

        public Category.CategoryEnum Category { get; set; }

        public virtual ICollection<User> Users { get; set; }
        public virtual ICollection<Lesson> Lessons { get; set; }
        public virtual ICollection<Application> Applications { get; set; }
    }
}
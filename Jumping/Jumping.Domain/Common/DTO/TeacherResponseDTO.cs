﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jumping.Domain.Common.DTO
{
    public class TeacherResponseDTO
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string RoleName { get; set; }
        public int RatingAsUser { get; set; }
        public int RatingAsTeacher { get; set; }
        public string Photo { get; set; }
        public string Portfolio { get; set; }
        public string DesiredTimeForLessons { get; set; }
    }
}
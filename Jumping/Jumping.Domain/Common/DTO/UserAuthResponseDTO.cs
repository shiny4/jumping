using System;
using Jumping.Domain.Enums;
using Jumping.Domain.Models;

namespace Jumping.Domain.Common.DTO
{
    public class UserAuthResponseDTO
    {
        public Guid Id { get; set; }
        public string Login { get; set; }
        public string Status { get; set; }

        //public User User { get; set; }
        //public NotificationPreference NotificationPreference { get; set; }
    }
}
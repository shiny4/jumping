﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Jumping.Domain.Enums;
using Jumping.Domain.Models;

namespace Jumping.Domain.Common.DTO
{
    public class CreateOrUpdateUserDTO
    {
        //private readonly CreateOrUpdateUserAuthDTO _createOrUpdateUserAuthDto = new CreateOrUpdateUserAuthDTO();
        public Guid UserAuthId { get; set; }
        public Guid NotificationPreferenceId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public int RatingAsUser { get; set; }
        public int RatingAsTeacher { get; set; }
        public string Photo { get; set; }
        public string Portfolio { get; set; }

        public string DesiredTimeForLessons { get; set; }
        //public NotificationPreference NotificationPreference { get; set; }
        //public UserAuth UserAuth { get; set; }
    }
}
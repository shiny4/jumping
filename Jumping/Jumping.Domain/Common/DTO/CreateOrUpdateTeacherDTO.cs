﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jumping.Domain.Common.DTO
{
    public class CreateOrUpdateTeacherDTO : CreateOrUpdateAdminDTO
    {
        public List<Guid> SubjectIds { get; set; }
        public List<Guid> LessonsAsStudentds { get; set; }
        public List<Guid> ReviewsByMeIds { get; set; }
        public List<Guid> InApplicatioIds { get; set; }
        public List<Guid> OutApplicationds { get; set; }
        public List<Guid> InSiteMessageds { get; set; }
        public List<Guid> OutSiteMessages { get; set; }
        public List<Guid> NotificationPreferences { get; set; }
        public List<Guid> CanceledLessons { get; set; }
        public List<Guid> LessonsAsTeacherIds { get; set; }
        public List<Guid> ReviewsOnMeIds { get; set; }
        public List<Guid> TicketIds { get; set; }

        public int RatingAsUser { get; set; }
        public int RatingAsTeacher { get; set; }
        public string Photo { get; set; }
        public string Portfolio { get; set; }
        public string DesiredTimeForLessons { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Jumping.Domain.Models;

namespace Jumping.Domain.Common.DTO
{
    public class UserResponseDTO
    {
        private readonly UserAuthResponseDTO _userAuthResponseDto = new UserAuthResponseDTO();
        public Guid Id { get; set; }
        public Guid UserAuthId { get; set; }
        public Guid NotificationPreferenceId { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public int RatingAsUser { get; set; }
        public int RatingAsTeacher { get; set; }
        public string Photo { get; set; }
        public string Portfolio { get; set; }
        public string DesiredTimeForLessons { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jumping.Domain.Common.DTO
{
    public class CreateOrUpdateTicketDTO
    {
        public Guid FromUserId { get; set; }
        public Guid? PreviosTicketId { get; set; }
        public string Theme { get; set; }
        public string Content { get; set; }
        public string AdminAnswer { get; set; }
    }
}
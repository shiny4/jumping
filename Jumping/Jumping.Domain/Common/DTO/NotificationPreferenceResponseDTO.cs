﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Jumping.Domain.Enums;

namespace Jumping.Domain.Common.DTO
{
    public class NotificationPreferenceResponseDTO
    {
        public Guid Id { get; set; }
        public Guid UserAuthId { get; set; }
        public string NoteAboutLesson { get; set; }
        public bool NoteAboutMessage { get; set; }
        public bool NoteAboutAnswer { get; set; }
    }
}
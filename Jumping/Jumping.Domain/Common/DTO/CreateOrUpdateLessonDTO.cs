﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Jumping.Domain.Enums;

namespace Jumping.Domain.Common.DTO
{
    public class CreateOrUpdateLessonDTO
    {
        public string LessonDescription { get; set; }
        public DateTime StartDate { get; set; }

        public int DurationInMin { get; set; }

        //public DateTime EndDate { get; set; }
        public DateTime? CancelDate { get; set; }
        public bool IsIndividual { get; set; }
        public LessonStatus Status { get; set; }
        public decimal? Price { get; set; }
        public string ActiveLink { get; set; }
        public Guid SubjectId { get; set; }
        public Guid TeacherId { get; set; }
    }
}
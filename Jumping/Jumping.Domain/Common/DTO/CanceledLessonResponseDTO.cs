﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Jumping.Domain.Enums;

namespace Jumping.Domain.Common.DTO
{
    public class CanceledLessonResponseDTO
    {
        public Guid Id { get; set; }
        public Guid LessonId { get; set; }
        public Guid InitiatorId { get; set; }
        public DateTime CancelDate { get; set; }
        public RoleEnum Role { get; set; }
        public string Reason { get; set; }
    }
}
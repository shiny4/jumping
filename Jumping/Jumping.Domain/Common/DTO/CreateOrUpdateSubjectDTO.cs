﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Jumping.Domain.Enums;

namespace Jumping.Domain.Common.DTO
{
    public class CreateOrUpdateSubjectDTO
    {
        public string Name { get; set; }
        public Category.CategoryEnum Category { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jumping.Domain.Common.DTO
{
    public class ApplicationResponseDTO
    {
        public Guid Id { get; set; }
        public string Description { get; set; }
        public Guid TeacherId { get; set; }
        public Guid StudentId { get; set; }
        public Guid? SubjectId { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime ActuallUntil { get; set; }
        public string DesiredDateTime { get; set; }
    }
}
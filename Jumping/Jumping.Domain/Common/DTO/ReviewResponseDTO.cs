﻿using Jumping.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jumping.Domain.Common.DTO
{
    public class ReviewResponseDTO
    {
        public Guid Id { get; set; }

        public string Description { get; set; }
        public Guid? AutherId { get; set; }
        public Guid OnWhomId { get; set; }
        public int Score { get; set; }
        public Guid LessonId { get; set; }
    }
}
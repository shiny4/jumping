﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jumping.Domain.Common.DTO
{
    public class CreateOrUpdateSiteMessageDTO
    {
        public Guid FromUserId { get; set; }
        public Guid ToUserId { get; set; }
        public Guid? PreviosMessageId { get; set; }
        public string Theme { get; set; }
        public string Content { get; set; }
    }
}
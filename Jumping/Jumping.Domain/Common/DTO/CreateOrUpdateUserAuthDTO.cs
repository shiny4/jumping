using System;
using Jumping.Domain.Enums;
using Jumping.Domain.Models;

namespace Jumping.Domain.Common.DTO
{
    public class CreateOrUpdateUserAuthDTO
    {
        public string Login { get; set; }
        public string Password { get; set; }
        public string PasswordSalt { get; set; }

        public UserStatusEnum Status { get; set; }

        //public User User { get; set; }
        //public NotificationPreference NotificationPreference { get; set; }
    }
}
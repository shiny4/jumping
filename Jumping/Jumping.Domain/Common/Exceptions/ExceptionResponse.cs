﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jumping.Domain.Common.Exceptions
{
    public class ExceptionResponse
    {
        public int? Status { get; set; }
        public string Message { get; set; }
    }
}
